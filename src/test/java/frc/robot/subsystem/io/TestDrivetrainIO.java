package frc.robot.subsystem.io;

import frc.robot.subsystem.drivetrain.io.DrivetrainIO;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.function.Supplier;

public class TestDrivetrainIO extends DrivetrainIO {

    private DrivetrainIOInputs testInputs;

    public TestDrivetrainIO(
            Supplier<Double> leftPercentSupplier,
            Supplier<Double> rightPercentSupplier,
            SystemToggleStore store) {
        super(leftPercentSupplier, rightPercentSupplier, store);
        testInputs = new DrivetrainIOInputs();
    }

    public DrivetrainIOInputs getDrivetrainIOInputs() {
        return testInputs;
    }

    public void setDrivetrainIOInputs(DrivetrainIOInputs inputs) {
        this.testInputs = inputs;
    }

    @Override
    public void updateInputsInternal(DrivetrainIOInputs inputs) {
        inputs = testInputs;
    }

    @Override
    public void updateDriveInternal() {}

    @Override
    protected void initialize() {}
}
