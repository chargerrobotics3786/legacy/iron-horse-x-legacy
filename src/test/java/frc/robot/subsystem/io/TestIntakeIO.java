package frc.robot.subsystem.io;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.robot.subsystem.intake.io.IntakeIO;
import frc.robot.util.systemtoggle.SystemToggleStore;

public class TestIntakeIO extends IntakeIO {

    private IntakeIOInputs inputs;

    public TestIntakeIO(SystemToggleStore store) {
        super(store);
        inputs = new IntakeIOInputs();
    }

    public void setIntakeIOInputs(IntakeIOInputs inputs) {
        this.inputs = inputs;
    }

    @Override
    protected void updateInputsInternal(IntakeIOInputs inputs) {
        inputs.setCollectorAppliedVolts(this.inputs.getCollectorAppliedVolts());
        inputs.setCollectorCurrentAmps(this.inputs.getCollectorCurrentAmps());
        inputs.setCollectorPositionRad(this.inputs.getCollectorPositionRad());
        inputs.setCollectorVelocityRadPerSec(this.inputs.getCollectorVelocityRadPerSec());
        inputs.setReedSwitchClosed(this.inputs.isReedSwitchClosed());
        inputs.setSolenoidValue(this.inputs.getSolenoidValue());
        inputs.setTransitionAppliedVolts(this.inputs.getTransitionAppliedVolts());
        inputs.setTransitionCurrentAmps(this.inputs.getTransitionCurrentAmps());
        inputs.setTransitionPositionRad(this.inputs.getTransitionPositionRad());
        inputs.setTransitionVelocityRadPerSec(this.inputs.getTransitionVelocityRadPerSec());
    }

    @Override
    protected void setSolenoidValueInternal(Value value) {
        inputs.setSolenoidValue(value);
    }

    @Override
    public void toggleSolenoidValueInternal() {
        inputs.setSolenoidValue(
                switch (inputs.getSolenoidValue()) {
                    case kForward -> Value.kReverse;
                    case kOff -> Value.kOff;
                    case kReverse -> Value.kForward;
                });
    }

    @Override
    protected void setCollectorValueInternal(double value) {
        inputs.setCollectorAppliedVolts(value * 12);
        inputs.setCollectorCurrentAmps(value * 30); // Approximate with a percentage of 30A
        inputs.setCollectorPositionRad(value);
        inputs.setCollectorVelocityRadPerSec(value);
    }

    @Override
    protected void setTransitionValueInternal(double value) {
        inputs.setTransitionAppliedVolts(value * 12);
        inputs.setTransitionCurrentAmps(value * 30); // Approximate with a percentage of 30A
        inputs.setTransitionPositionRad(value);
        inputs.setTransitionVelocityRadPerSec(value);
    }

    @Override
    protected void initialize() {}
}
