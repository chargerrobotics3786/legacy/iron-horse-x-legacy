package frc.robot.subsystem;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import frc.robot.subsystem.intake.IntakeSubsystem;
import frc.robot.subsystem.intake.IntakeSubsystem.IntakeState;
import frc.robot.subsystem.intake.io.IntakeIO.IntakeIOInputs;
import frc.robot.subsystem.io.TestIntakeIO;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.io.IOType;
import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class TestIntakeSubsystem {

    @Test
    void periodicUpdatesState() {
        SystemToggleStore toggleStore = mock(SystemToggleStore.class);
        when(toggleStore.isToggledOn(SystemToggle.INTAKE)).thenReturn(Optional.of(true));
        FeatureFlagStore featureFlagStore = mock(FeatureFlagStore.class);
        when(featureFlagStore.isEnabled(FeatureFlag.INTAKE_ENABLED)).thenReturn(Optional.of(true));
        TestIntakeIO testIntakeIO = new TestIntakeIO(toggleStore);
        IntakeIOInputs testInputs = new IntakeIOInputs();
        testInputs.setReedSwitchClosed(true);
        testIntakeIO.setIntakeIOInputs(testInputs);
        IntakeSubsystem intakeSubsystem =
                new IntakeSubsystem(
                        IOType.SIMULATION,
                        Map.of(IOType.SIMULATION, testIntakeIO),
                        featureFlagStore);
        intakeSubsystem.setDesiredState(IntakeState.EXTENDED_RUNNING);
        intakeSubsystem.periodic();
        assertEquals(
                IntakeState.EXTENDED,
                intakeSubsystem.getCurrentState(),
                "Intake Subsystem state should be extended and off by default");
    }
}
