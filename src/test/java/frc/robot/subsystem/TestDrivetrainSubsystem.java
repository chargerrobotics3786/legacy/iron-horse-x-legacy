package frc.robot.subsystem;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import edu.wpi.first.networktables.BooleanTopic;
import edu.wpi.first.networktables.NetworkTableInstance;
import frc.robot.subsystem.drivetrain.DrivetrainSubsystem;
import frc.robot.subsystem.drivetrain.io.DrivetrainIO.DrivetrainIOInputs;
import frc.robot.subsystem.io.TestDrivetrainIO;
import frc.robot.util.io.IOType;
import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TestDrivetrainSubsystem {

    @Mock public Supplier<Double> leftSupplierMock;
    @Mock public Supplier<Double> rightSupplierMock;
    @Mock public SystemToggleStore toggleStore;

    private TestDrivetrainIO testDrivetrainIO;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        testDrivetrainIO =
                spy(new TestDrivetrainIO(rightSupplierMock, leftSupplierMock, toggleStore));
        when(toggleStore.isToggledOn(SystemToggle.DRIVETRAIN)).thenReturn(Optional.of(true));
    }

    @Test
    void periodicShouldUtilizeSuppliers() {
        BooleanTopic topic = NetworkTableInstance.getDefault().getBooleanTopic("test");
        DrivetrainSubsystem drivetrainSubsystem =
                new DrivetrainSubsystem(
                        IOType.SIMULATION,
                        Map.of(IOType.SIMULATION, testDrivetrainIO),
                        topic.publish(),
                        topic.subscribe(false));

        drivetrainSubsystem.periodic();

        verify(testDrivetrainIO).updateDrive();
    }

    @Test
    void periodicUpdatesInputs() {
        BooleanTopic topic = NetworkTableInstance.getDefault().getBooleanTopic("test");
        TestDrivetrainIO drivetrainIO =
                new TestDrivetrainIO(rightSupplierMock, leftSupplierMock, toggleStore);
        DrivetrainSubsystem drivetrainSubsystem =
                new DrivetrainSubsystem(
                        IOType.SIMULATION,
                        Map.of(IOType.SIMULATION, drivetrainIO),
                        topic.publish(),
                        topic.subscribe(false));

        DrivetrainIOInputs inputs = drivetrainIO.getDrivetrainIOInputs();

        inputs.setLeftAppliedVolts(inputs.getLeftAppliedVolts() + 10.9);

        drivetrainSubsystem.periodic();

        assertEquals(
                inputs,
                drivetrainIO.getDrivetrainIOInputs(),
                "Inputs must be updated when periodic gets run");
    }
}
