package frc.robot.util;

import frc.robot.BuildConstants;
import frc.robot.util.io.IOType;
import org.littletonrobotics.junction.LogFileUtil;
import org.littletonrobotics.junction.Logger;
import org.littletonrobotics.junction.networktables.NT4Publisher;
import org.littletonrobotics.junction.wpilog.WPILOGReader;
import org.littletonrobotics.junction.wpilog.WPILOGWriter;

public class AdvantageKitUtils {
    private static boolean setupComplete = false;

    private AdvantageKitUtils() {
        // Empty constructor to hide public auto created one.
    }

    private static void setupRuntimeLogging() {
        Logger.addDataReceiver(new WPILOGWriter("/u"));
        Logger.addDataReceiver(new NT4Publisher());
    }

    private static void setupSimulationLogging() {
        Logger.addDataReceiver(new NT4Publisher());
    }

    private static void setupReplayLogging() {
        String logPath = LogFileUtil.findReplayLog();
        Logger.setReplaySource(new WPILOGReader(logPath));
        Logger.addDataReceiver(new WPILOGWriter(LogFileUtil.addPathSuffix(logPath, "_sim")));
    }

    /**
     * Perform initial system setup for AdvantageKit
     *
     * @param ioType The type of IO to set the robot up for
     */
    public static void initialSetup(IOType ioType) {
        if (setupComplete) {
            Logger.recordOutput("ERROR", "Attempted to reinitialize logging");
            return;
        }

        Logger.recordMetadata("ProjectName", BuildConstants.MAVEN_NAME);
        Logger.recordMetadata("BuildDate", BuildConstants.BUILD_DATE);
        Logger.recordMetadata("GitSHA", BuildConstants.GIT_SHA);
        Logger.recordMetadata("GitDate", BuildConstants.GIT_DATE);
        Logger.recordMetadata("GitBranch", BuildConstants.GIT_BRANCH);
        String dirtyKey = "GitDirty";
        switch (BuildConstants.DIRTY) {
            case 0 -> Logger.recordMetadata(dirtyKey, "All changes committed");
            case 1 -> Logger.recordMetadata(dirtyKey, "Uncomitted changes");
            default -> Logger.recordMetadata(dirtyKey, "Unknown");
        }

        switch (ioType) {
            case REAL -> AdvantageKitUtils.setupRuntimeLogging();
            case SIMULATION -> AdvantageKitUtils.setupSimulationLogging();
            case REPLAY -> AdvantageKitUtils.setupReplayLogging();
        }

        Logger.start();

        setupComplete = true;
    }
}
