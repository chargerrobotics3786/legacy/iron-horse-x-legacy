package frc.robot.util.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import edu.wpi.first.math.filter.SlewRateLimiter;
import frc.robot.subsystem.drivetrain.io.DrivetrainIO;
import frc.robot.subsystem.drivetrain.io.DrivetrainIOReplay;
import frc.robot.subsystem.drivetrain.io.DrivetrainIOSim;
import frc.robot.subsystem.drivetrain.io.DrivetrainIOSparkMax;
import frc.robot.subsystem.hood.io.HoodIO;
import frc.robot.subsystem.hood.io.HoodIOReplay;
import frc.robot.subsystem.hood.io.HoodIOSim;
import frc.robot.subsystem.hood.io.HoodIOSparkMaxCANCoder;
import frc.robot.subsystem.intake.io.IntakeIO;
import frc.robot.subsystem.intake.io.IntakeIOReplay;
import frc.robot.subsystem.intake.io.IntakeIOSim;
import frc.robot.subsystem.intake.io.IntakeIOSparkMaxPCM;
import frc.robot.subsystem.shooter.io.ShooterIO;
import frc.robot.subsystem.shooter.io.ShooterIOReplay;
import frc.robot.subsystem.shooter.io.ShooterIOSim;
import frc.robot.subsystem.shooter.io.ShooterIOSparkMax;
import frc.robot.util.Constants;
import frc.robot.util.controlscheme.ControlSchemeService;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.io.IOType;
import java.util.Map;
import java.util.function.Supplier;

public class IOModule extends AbstractModule {

    @Provides
    public SlewRateLimiter driveSlewRateLimiter() {
        return new SlewRateLimiter(
                Constants.Drivetrain.POSITIVE_RATE_LIMIT,
                Constants.Drivetrain.NEGATIVE_RATE_LIMIT,
                0);
    }

    @Provides
    @Named("drivetrainLeftControlSupplier")
    public Supplier<Double> provideDrivetrainLeft(
            SlewRateLimiter slewRateLimiter,
            ControlSchemeService controlSchemeService,
            FeatureFlagStore featureFlagStore) {
        return () -> {
            if (featureFlagStore.isEnabled(FeatureFlag.DRIVETRAIN_ENABLED).orElse(false)) {
                return slewRateLimiter.calculate(
                        controlSchemeService.getCurrentControlScheme().getLeftDriveAxis());
            }
            return 0.0;
        };
    }

    @Provides
    @Named("drivetrainRightControlSupplier")
    public Supplier<Double> provideDrivetrainRight(
            SlewRateLimiter slewRateLimiter,
            ControlSchemeService controlSchemeService,
            FeatureFlagStore featureFlagStore) {
        return () -> {
            if (featureFlagStore.isEnabled(FeatureFlag.DRIVETRAIN_ENABLED).orElse(false)) {
                return slewRateLimiter.calculate(
                        controlSchemeService.getCurrentControlScheme().getRightDriveAxis());
            }
            return 0.0;
        };
    }

    @Provides
    public Map<IOType, DrivetrainIO> drivetrainIO(
            DrivetrainIOReplay replay, DrivetrainIOSim sim, DrivetrainIOSparkMax sparkMax) {
        return Map.of(
                IOType.REPLAY, replay,
                IOType.SIMULATION, sim,
                IOType.REAL, sparkMax);
    }

    @Provides
    public Map<IOType, HoodIO> hoodIO(
            HoodIOReplay replay, HoodIOSim sim, HoodIOSparkMaxCANCoder sparkMaxCANCoder) {
        return Map.of(
                IOType.REPLAY, replay,
                IOType.SIMULATION, sim,
                IOType.REAL, sparkMaxCANCoder);
    }

    @Provides
    public Map<IOType, IntakeIO> intakeIO(
            IntakeIOReplay replay, IntakeIOSim sim, IntakeIOSparkMaxPCM sparkMaxPCM) {
        return Map.of(
                IOType.REPLAY, replay,
                IOType.SIMULATION, sim,
                IOType.REAL, sparkMaxPCM);
    }

    @Provides
    public Map<IOType, ShooterIO> shooterIO(
            ShooterIOReplay replay, ShooterIOSim sim, ShooterIOSparkMax sparkMax) {
        return Map.of(
                IOType.REPLAY, replay,
                IOType.SIMULATION, sim,
                IOType.REAL, sparkMax);
    }
}
