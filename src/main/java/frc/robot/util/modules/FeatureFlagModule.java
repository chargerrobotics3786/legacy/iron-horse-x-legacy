package frc.robot.util.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.featureflag.NTFeatureFlagStore;

public class FeatureFlagModule extends AbstractModule {

    @Provides
    public FeatureFlagStore featureFlagStore() {
        return new NTFeatureFlagStore();
    }
}
