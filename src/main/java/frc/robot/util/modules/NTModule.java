package frc.robot.util.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Named;
import edu.wpi.first.networktables.BooleanPublisher;
import edu.wpi.first.networktables.BooleanSubscriber;
import edu.wpi.first.networktables.BooleanTopic;
import edu.wpi.first.networktables.NetworkTableInstance;

public class NTModule extends AbstractModule {

    @Provides
    @Named("resetRobotPoseTopic")
    public BooleanTopic resetRobotPoseTopic() {
        return NetworkTableInstance.getDefault().getBooleanTopic("Reset Robot Pose");
    }

    @Provides
    @Named("resetRobotPosePublisher")
    public BooleanPublisher resetRobotPosePublisher(
            @Named("resetRobotPoseTopic") BooleanTopic resetTopic) {
        return resetTopic.publish();
    }

    @Provides
    @Named("resetRobotPoseSubscriber")
    public BooleanSubscriber resetRobotPoseSubscriber(
            @Named("resetRobotPoseTopic") BooleanTopic resetTopic) {
        return resetTopic.subscribe(false);
    }
}
