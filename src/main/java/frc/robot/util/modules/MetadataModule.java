package frc.robot.util.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj.RobotBase;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;
import frc.robot.util.io.IOType;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Data;

public class MetadataModule extends AbstractModule {

    @Provides
    public IOType ioType() {
        if (RobotBase.isSimulation()) {
            return IOType.SIMULATION;
        }

        if (Robot.IS_REPLAY) {
            return IOType.REPLAY;
        }

        return IOType.REAL;
    }

    @Provides
    @Singleton
    public Set<SubsystemBase> provideAllSubsystems(Injector injector) {
        return injector.getAllBindings().entrySet().stream()
                .filter(
                        entry ->
                                SubsystemBase.class.isAssignableFrom(
                                        entry.getKey().getTypeLiteral().getRawType()))
                .map(entry -> (SubsystemBase) entry.getValue().getProvider().get())
                .collect(Collectors.toSet());
    }

    @Data
    public static class PeriodProvider {
        private double period;
    }

    @Provides
    @Singleton
    public PeriodProvider periodProvider() {
        return new PeriodProvider();
    }
}
