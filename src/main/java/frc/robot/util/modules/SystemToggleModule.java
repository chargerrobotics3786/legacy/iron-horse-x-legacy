package frc.robot.util.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import frc.robot.util.systemtoggle.DeploySystemToggleStore;
import frc.robot.util.systemtoggle.SystemToggleStore;

public class SystemToggleModule extends AbstractModule {

    @Provides
    public SystemToggleStore systemToggleStore() {
        return new DeploySystemToggleStore();
    }
}
