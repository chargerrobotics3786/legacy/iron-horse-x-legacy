package frc.robot.util.controlscheme;

import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.util.Constants;

/**
 * Implements the following XBox Controller bindings:
 *
 * <p>A - B - Run Intake in reverse X - Y -
 *
 * <p>LB - Toggle Intake running/stopped RB - LT - RT - Run Shooter at speed and fire
 *
 * <p>POV Up - POV Right - POV Down - POV Left -
 *
 * <p>View - Select -
 *
 * <p>Left Stick Y - Left motor speed (configured in DriveSubsystem) Left Stick X - Left Stick -
 *
 * <p>Right Stick Y - Right motor speed (configured in DriveSubsystem) Right Stick X - Right Stick -
 */
public class XBoxControllerScheme implements ControlScheme {
    private final CommandXboxController driveController;

    public XBoxControllerScheme() {
        driveController = new CommandXboxController(Constants.Controls.ControllerPorts.XBOX);
    }

    @Override
    public double getRightDriveAxis() {
        return driveController.getRightY();
    }

    @Override
    public double getLeftDriveAxis() {
        return driveController.getLeftY();
    }

    @Override
    public Trigger getIntakeToggle() {
        return driveController.leftBumper();
    }

    @Override
    public Trigger getIntakeReverse() {
        return driveController.b();
    }

    @Override
    public Trigger getFiringButton() {
        return driveController.rightTrigger(Constants.Controls.FLYWHEEL_FIRING_THRESHOLD);
    }

    @Override
    public Trigger getLowAngle() {
        return driveController.povUp();
    }

    @Override
    public Trigger getMidAngle() {
        return driveController.povLeft();
    }

    @Override
    public Trigger getHighAngle() {
        return driveController.povDown();
    }
}
