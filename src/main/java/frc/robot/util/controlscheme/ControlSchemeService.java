package frc.robot.util.controlscheme;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.command.CommandService;
import frc.robot.util.Constants;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import lombok.Getter;
import org.littletonrobotics.junction.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class ControlSchemeService {

    private static final org.slf4j.Logger LOGGER =
            LoggerFactory.getLogger(ControlSchemeService.class);

    @Getter private ControlSchemeOption currentlySelectedScheme;

    @Getter private ControlScheme currentControlScheme;

    private final CommandService commandService;

    private final SendableChooser<ControlSchemeOption> controlChooser;
    private boolean schemeLocked;

    @Inject
    public ControlSchemeService(CommandService commandService) {
        this.commandService = commandService;
        this.schemeLocked = false;
        this.setControlScheme(Constants.Controls.DEFAULT_CONTROL_SCHEME);
        controlChooser = new SendableChooser<>();
        List.of(ControlSchemeOption.values()).stream()
                .forEach(option -> controlChooser.addOption(option.name(), option));
        controlChooser.setDefaultOption(
                Constants.Controls.DEFAULT_CONTROL_SCHEME.name(),
                Constants.Controls.DEFAULT_CONTROL_SCHEME);
        controlChooser.onChange(this::setControlScheme);
        Shuffleboard.getTab("controls").add("Control Scheme", controlChooser);
    }

    private ControlScheme getControlSchemeFromOption() {
        try {
            return currentlySelectedScheme.getSchemeClass().getDeclaredConstructor().newInstance();
        } catch (InstantiationException
                | IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException
                | NoSuchMethodException
                | SecurityException e) {
            LOGGER.error(
                    "Failed to create control scheme from currently selected option. Making no switch.",
                    e);
            return currentControlScheme;
        }
    }

    public void setControlScheme(ControlSchemeOption controlSchemeOption) {
        this.currentlySelectedScheme = controlSchemeOption;
        this.currentControlScheme = getControlSchemeFromOption();
        // Clear bindings after updating scheme to avoid a case where no bindings are present
        CommandScheduler.getInstance().getDefaultButtonLoop().clear();
        configureBindings();
        LOGGER.atInfo()
                .setMessage("Switching to {} controls")
                .addArgument(controlSchemeOption.name())
                .log();
        Logger.recordOutput("ControlScheme", controlSchemeOption);
        if (schemeLocked) {
            unlockScheme();
        }
    }

    private void configureBindings() {
        currentControlScheme.getIntakeToggle().onTrue(commandService.getToggleIntakeCommand());
        currentControlScheme.getIntakeReverse().onTrue(commandService.getReverseIntakeCommand());
        currentControlScheme
                .getFiringButton()
                .onTrue(commandService.getShooterToSpeedCommand())
                .onFalse(commandService.getShooterIdleCommand());
        currentControlScheme.getLowAngle().onTrue(commandService.getHoodToLowCommand());
        currentControlScheme.getMidAngle().onTrue(commandService.getHoodToMidCommand());
        currentControlScheme.getHighAngle().onTrue(commandService.getHoodToHighCommand());
    }

    private void lockScheme() {
        if (!schemeLocked) {
            schemeLocked = true;
            this.controlChooser.onChange(
                    cso -> LOGGER.warn("Cannot change control scheme during testing."));
        }
    }

    private void unlockScheme() {
        if (schemeLocked) {
            schemeLocked = false;
            this.controlChooser.onChange(this::setControlScheme);
        }
    }

    public ControlSchemeOption setTestScheme(TestModeScheme testModeScheme) {
        ControlSchemeOption lastSetOption = this.currentlySelectedScheme;

        Logger.recordOutput("ControlScheme", "--Test Scheme--");

        this.currentControlScheme = testModeScheme;
        this.lockScheme();
        CommandScheduler.getInstance().getDefaultButtonLoop().clear();
        configureBindings();
        return lastSetOption;
    }
}
