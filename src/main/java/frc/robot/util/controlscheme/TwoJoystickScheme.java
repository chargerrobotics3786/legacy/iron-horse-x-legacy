package frc.robot.util.controlscheme;

import edu.wpi.first.wpilibj2.command.button.CommandJoystick;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.util.Constants;

/**
 * Implements the following XBox Controller bindings:
 *
 * <p>Right Stick Trigger - Run Shooter at speed and fire
 *
 * <p>Left Stick Trigger - Toggle Intake running/stopped
 *
 * <p>POV Down - Run Intake in reverse
 *
 * <p>Left Stick Y - Left motor speed (configured in DriveSubsystem)
 *
 * <p>Right Stick Y - Right motor speed (configured in DriveSubsystem)
 */
public class TwoJoystickScheme implements ControlScheme {
    private final CommandJoystick leftStick;
    private final CommandJoystick rightStick;

    public TwoJoystickScheme() {
        this.leftStick = new CommandJoystick(Constants.Controls.ControllerPorts.JOYSTICK_ONE);
        this.rightStick = new CommandJoystick(Constants.Controls.ControllerPorts.JOYSTICK_TWO);
    }

    @Override
    public double getRightDriveAxis() {
        return rightStick.getY();
    }

    @Override
    public double getLeftDriveAxis() {
        return leftStick.getY();
    }

    @Override
    public Trigger getIntakeToggle() {
        return leftStick.trigger();
    }

    @Override
    public Trigger getIntakeReverse() {
        return leftStick.povDown();
    }

    @Override
    public Trigger getFiringButton() {
        return rightStick.trigger();
    }

    @Override
    public Trigger getLowAngle() {
        return rightStick.povUp();
    }

    @Override
    public Trigger getMidAngle() {
        return rightStick.povLeft();
    }

    @Override
    public Trigger getHighAngle() {
        return rightStick.povDown();
    }
}
