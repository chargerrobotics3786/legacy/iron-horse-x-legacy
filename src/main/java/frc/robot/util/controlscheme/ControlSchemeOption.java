package frc.robot.util.controlscheme;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ControlSchemeOption {
    XBOX(XBoxControllerScheme.class),
    DUAL_STICKS(TwoJoystickScheme.class);

    @Getter private Class<? extends ControlScheme> schemeClass;
}
