package frc.robot.util.controlscheme;

import edu.wpi.first.wpilibj2.command.button.Trigger;
import lombok.Setter;
import org.littletonrobotics.junction.Logger;

public class TestModeScheme implements ControlScheme {

    @Setter private double currentRightDrive;
    @Setter private double currentLeftDrive;
    @Setter private boolean intakeShouldToggle;
    @Setter private boolean intakeShouldReverse;
    @Setter private boolean shooterShouldFire;
    @Setter private boolean setLowAngle;
    @Setter private boolean setMidAngle;
    @Setter private boolean setHighAngle;

    public TestModeScheme(double initialRightDrive, double initialLeftDrive) {
        currentRightDrive = initialRightDrive;
        currentLeftDrive = initialLeftDrive;
        this.intakeShouldToggle = false;
        this.intakeShouldReverse = false;
        this.shooterShouldFire = false;
        this.setLowAngle = false;
        this.setMidAngle = false;
        this.setHighAngle = false;
    }

    @Override
    public double getRightDriveAxis() {
        Logger.recordOutput("Controls/RightAxis", currentRightDrive);
        return currentRightDrive;
    }

    @Override
    public double getLeftDriveAxis() {
        Logger.recordOutput("Controls/LeftAxis", currentLeftDrive);
        return currentLeftDrive;
    }

    @Override
    public Trigger getIntakeToggle() {
        return new Trigger(
                () -> {
                    if (intakeShouldToggle) {
                        intakeShouldToggle = false;
                        Logger.recordOutput("Controls/ToggleIntake", true);
                        return true;
                    }
                    Logger.recordOutput("Controls/ToggleIntake", false);
                    return false;
                });
    }

    @Override
    public Trigger getIntakeReverse() {
        return new Trigger(
                () -> {
                    Logger.recordOutput("Controls/ReverseIntake", intakeShouldReverse);
                    return intakeShouldReverse;
                });
    }

    @Override
    public Trigger getFiringButton() {
        return new Trigger(
                () -> {
                    Logger.recordOutput("Controls/ShooterFire", shooterShouldFire);
                    return shooterShouldFire;
                });
    }

    @Override
    public Trigger getLowAngle() {
        return new Trigger(
                () -> {
                    Logger.recordOutput("Controls/LowAngle", setLowAngle);
                    return setLowAngle;
                });
    }

    @Override
    public Trigger getMidAngle() {
        return new Trigger(
                () -> {
                    Logger.recordOutput("Controls/MidAngle", setMidAngle);
                    return setMidAngle;
                });
    }

    @Override
    public Trigger getHighAngle() {
        return new Trigger(
                () -> {
                    Logger.recordOutput("Controls/HighAngle", setHighAngle);
                    return setHighAngle;
                });
    }
}
