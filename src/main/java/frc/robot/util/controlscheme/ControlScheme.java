package frc.robot.util.controlscheme;

import edu.wpi.first.wpilibj2.command.button.Trigger;

public interface ControlScheme {

    public double getRightDriveAxis();

    public double getLeftDriveAxis();

    public Trigger getIntakeToggle();

    public Trigger getIntakeReverse();

    public Trigger getFiringButton();

    public Trigger getLowAngle();

    public Trigger getMidAngle();

    public Trigger getHighAngle();
}
