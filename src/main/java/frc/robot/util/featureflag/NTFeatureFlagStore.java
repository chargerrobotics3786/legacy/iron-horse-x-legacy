package frc.robot.util.featureflag;

import edu.wpi.first.networktables.BooleanEntry;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NTFeatureFlagStore implements FeatureFlagStore {
    private static final String FEATURE_FLAG_TABLE_NAME = "FeatureFlag";

    private NetworkTable featureFlagTable;

    private Map<FeatureFlag, BooleanEntry> flagEntries;

    public NTFeatureFlagStore() {
        featureFlagTable = NetworkTableInstance.getDefault().getTable(FEATURE_FLAG_TABLE_NAME);

        flagEntries =
                Stream.of(FeatureFlag.values())
                        .collect(
                                Collectors.toMap(
                                        flag -> flag,
                                        flag ->
                                                featureFlagTable
                                                        .getBooleanTopic(flag.getName())
                                                        .getEntry(flag.isDefaultEnabled())));

        flagEntries.forEach((ff, entry) -> entry.set(ff.isDefaultEnabled()));
    }

    @Override
    public Optional<Boolean> isEnabled(FeatureFlag flag) {
        if (!featureFlagTable.containsKey(flag.getName())) {
            return Optional.empty();
        }
        return Optional.of(flagEntries.get(flag).getAsBoolean());
    }

    @Override
    public void enable(FeatureFlag flag) {
        flagEntries.get(flag).set(true);
    }

    @Override
    public void disable(FeatureFlag flag) {
        flagEntries.get(flag).set(false);
    }
}
