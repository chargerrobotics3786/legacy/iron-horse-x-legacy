package frc.robot.util.featureflag;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum FeatureFlag {
    DRIVETRAIN_ENABLED("drivetrain-enabled", true),
    SHOOTER_ENABLED("shooter-enabled", false),
    HOOD_ENABLED("hood-enabled", false),
    INTAKE_ENABLED("intake-enabled", false),
    TRAINING_MODE("training-mode", false);

    private String name;
    private boolean defaultEnabled;
}
