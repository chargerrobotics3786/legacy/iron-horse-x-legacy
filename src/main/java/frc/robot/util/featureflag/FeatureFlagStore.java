package frc.robot.util.featureflag;

import java.util.Optional;

public interface FeatureFlagStore {

    /**
     * @param flag The flag to check the status of
     * @return Whether or not the flag is enabled, null if the flag doesn't exist in the store
     */
    public Optional<Boolean> isEnabled(FeatureFlag flag);

    public void enable(FeatureFlag flag);

    public void disable(FeatureFlag flag);
}
