package frc.robot.util.io;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class IOSubsystem<I> extends SubsystemBase {
    protected I subsystemIO;
}
