package frc.robot.util.io;

public enum IOType {
    REAL,
    SIMULATION,
    REPLAY
}
