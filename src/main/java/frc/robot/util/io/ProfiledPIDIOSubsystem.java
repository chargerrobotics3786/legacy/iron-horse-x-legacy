package frc.robot.util.io;

import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.wpilibj2.command.ProfiledPIDSubsystem;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class ProfiledPIDIOSubsystem<I> extends ProfiledPIDSubsystem {
    protected I subsystemIO;

    protected ProfiledPIDIOSubsystem(ProfiledPIDController profiledPIDController, I subsystemIO) {
        super(profiledPIDController);
        this.subsystemIO = subsystemIO;
    }
}
