package frc.robot.util.systemtoggle;

import lombok.Getter;

public abstract class ToggleableIO {

    @Getter private final boolean isToggledOn;

    protected ToggleableIO(SystemToggle toggle, SystemToggleStore store) {
        this.isToggledOn = store.isToggledOn(toggle).orElse(false);
        if (isToggledOn) {
            initialize();
        }
    }

    protected abstract void initialize();
}
