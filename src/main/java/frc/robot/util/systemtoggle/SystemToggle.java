package frc.robot.util.systemtoggle;

public enum SystemToggle {
    DRIVETRAIN,
    HOOD,
    INTAKE,
    SHOOTER;
}
