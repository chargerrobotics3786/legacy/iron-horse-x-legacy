package frc.robot.util.systemtoggle;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.wpi.first.wpilibj.Filesystem;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeploySystemToggleStore implements SystemToggleStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeploySystemToggleStore.class);

    private static final String DEPLOY_FILE_NAME = "systemToggleStore.json";

    private final Map<SystemToggle, Boolean> toggleMap;

    public DeploySystemToggleStore() {
        toggleMap = readDeployFile();
        LOGGER.atInfo().setMessage("Loaded System Toggles: {}").addArgument(toggleMap).log();
    }

    private Map<SystemToggle, Boolean> readDeployFile() {
        File deployDir = Filesystem.getDeployDirectory();
        File toggleStoreFile =
                new File(
                        String.join(File.separator, deployDir.getAbsolutePath(), DEPLOY_FILE_NAME));

        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(
                    toggleStoreFile, new TypeReference<Map<SystemToggle, Boolean>>() {});
        } catch (IOException e) {
            LOGGER.error("Failed to read toggle store file", e);
        }
        return Map.of();
    }

    @Override
    public Optional<Boolean> isToggledOn(SystemToggle toggle) {
        return toggleMap.containsKey(toggle)
                ? Optional.of(toggleMap.get(toggle))
                : Optional.empty();
    }
}
