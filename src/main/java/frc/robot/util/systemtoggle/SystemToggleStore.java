package frc.robot.util.systemtoggle;

import java.util.Optional;

public interface SystemToggleStore {

    /**
     * @param toggle The toggle to check the status of
     * @return Whether or not the toggle is enabled, null if the toggle doesn't exist in the store
     */
    public Optional<Boolean> isToggledOn(SystemToggle toggle);
}
