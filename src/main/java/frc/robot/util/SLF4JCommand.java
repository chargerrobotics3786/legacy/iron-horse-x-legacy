package frc.robot.util;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.slf4j.spi.LoggingEventBuilder;

/** Utility command to log a message with SLF4j */
public class SLF4JCommand extends InstantCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(SLF4JCommand.class);

    private final Level level;
    private String message;
    private List<Object> args;

    public SLF4JCommand(Level level) {
        this.level = level;
    }

    public SLF4JCommand(Level level, String message, Object... args) {
        this(level);
        this.message = message;
        this.args = List.of(args);
    }

    public SLF4JCommand setMessage(String message) {
        this.message = message;
        return this;
    }

    public SLF4JCommand addArgument(Object arg) {
        this.args.add(arg);
        return this;
    }

    public SLF4JCommand addArguments(Object... args) {
        return addArguments(List.of(args));
    }

    public SLF4JCommand addArguments(List<Object> args) {
        this.args.addAll(args);
        return this;
    }

    public SLF4JCommand setArguments(Object... args) {
        return setArguments(List.of(args));
    }

    public SLF4JCommand setArguments(List<Object> args) {
        this.args = args;
        return this;
    }

    @Override
    public void initialize() {
        if (this.message == null) {
            LOGGER.atError()
                    .setMessage("SLF4JCommand run without a message")
                    .setCause(new RuntimeException("SLF4JCommand missing message"))
                    .log();
            return;
        }

        LoggingEventBuilder loggingEventBuilder = LOGGER.atLevel(level).setMessage(message);
        args.stream().forEach(loggingEventBuilder::addArgument);

        loggingEventBuilder.log();
    }
}
