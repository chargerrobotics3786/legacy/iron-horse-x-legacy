package frc.robot.util;

import edu.wpi.first.math.VecBuilder;
import edu.wpi.first.math.Vector;
import edu.wpi.first.math.numbers.N7;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim.KitbotGearing;
import frc.robot.util.controlscheme.ControlSchemeOption;
import org.ejml.simple.UnsupportedOperation;

public class Constants {
    private class ConstantClass {
        private ConstantClass() {
            throw new UnsupportedOperation(
                    "%s is a purely static class".formatted(getClass().getName()));
        }
    }

    private Constants() {
        throw new UnsupportedOperation("Constants is a purely static class");
    }

    public class Controls extends ConstantClass {
        public static final double FLYWHEEL_FIRING_THRESHOLD = 0.5;
        public static final ControlSchemeOption DEFAULT_CONTROL_SCHEME = ControlSchemeOption.XBOX;

        public class ControllerPorts extends ConstantClass {
            public static final int XBOX = 0;
            public static final int JOYSTICK_ONE = 1;
            public static final int JOYSTICK_TWO = 2;
        }
    }

    public class Drivetrain extends ConstantClass {
        public class PhysicalCharacteristics extends ConstantClass {
            /** Gear ratio between the drive motor and output wheel. Units: none */
            public static final double DRIVE_GEAR_RATIO = KitbotGearing.k10p71.value;

            /** Moment of inertia of the robot about the center. Units: Kg * m^2 */
            public static final double ROBOT_MOMENT_OF_INERTIA = 7.5;

            /** Mass of the robot. Units: Kg */
            public static final double ROBOT_MASS = 60.0;

            /** Radius of the robot wheels. Units: Meters */
            public static final double WHEEL_RADIUS = Units.inchesToMeters(3);

            /** Width of the robot track. Units: Meters */
            public static final double TRACK_WIDTH = 0.7112;

            /** Standard deviations of noise for measurements. Units: various */
            public static final Vector<N7> MEASUREMENT_NOISE_STD_DEVS =
                    VecBuilder.fill(
                            0.001, // X position in meters
                            0.001, // Y position in meters
                            0.001, // Heading in radians
                            0.1, // Left velocity in m/s
                            0.1, // Right velocity in m/s
                            0.005, // Left position in meters
                            0.005 // Right position in meters
                            );
        }

        /** Max speed factor for each drive motor. Range: 0-1 */
        public static final double MAX_SPEED = 0.5;

        /** CAN Id of the motor on the lefthand side of the drivetrain, as viewed from the back. */
        public static final int LEFT_DRIVE_MOTOR_ID = 11;

        /** CAN Id of the motor on the righthand side of the drivetrain, as viewed from the back. */
        public static final int RIGHT_DRIVE_MOTOR_ID = 12;

        public static final double POSITIVE_RATE_LIMIT = 0.5;

        public static final double NEGATIVE_RATE_LIMIT = -0.75;
    }

    public class Intake extends ConstantClass {
        public class PhysicalCharacteristics extends ConstantClass {
            /** Gear ratio between the collector motor and the belt shaft. Units: none */
            public static final double COLLECTOR_GEAR_RATIO = 60;

            /** Gear ratio between the transition motor and the belt shaft. Units: none */
            public static final double TRANSITION_GEAR_RATIO = 50;
        }

        /**
         * Channel on the pneumatics controller that, when energized, moves the intake outward from
         * the robot
         */
        public static final int FORWARD_PNEUMATICS_CHANNEL = 0;

        /**
         * Channel on the pneumatics controller that, when energized, moves the intake into the
         * robot
         */
        public static final int REVERSE_PNEUMATICS_CHANNEL = 1;

        /** Digital I/O channel that the intake reed switch is connected to */
        public static final int REED_SWITCH_DIGITAL_CHANNEL = 0;

        /** CAN Id of the motor that drives the belts of the collector portion of the intake. */
        public static final int COLLECTOR_BELT_MOTOR_ID = 21;

        /** CAN Id of the motor that drives the belts of the transition portion of the intake. */
        public static final int TRANSITION_BELT_MOTOR_ID = 22;

        /** Speed at which to drive the collector belts under normal operation. */
        public static final double COLLECTOR_BELT_SPEED = 0.5;

        /** Maximum current to allow the collector motor to draw continuously. */
        public static final int COLLECTOR_CURRENT_LIMIT_AMPS = 20;

        /** Speed at which to drive the transition belts under normal operation. */
        public static final double TRANSITION_BELT_SPEED = 0.5;

        /** Maximum current to allow the transition motor to draw continuously. */
        public static final int TRANSITION_CURRENT_LIMIT_AMPS = 20;

        /** Allowed variance from 0 for belts to be considered off */
        public static final double BELT_SPEED_DEAD_ZONE = 0.02;
    }

    public class Shooter extends ConstantClass {
        public class PhysicalCharacteristics extends ConstantClass {
            /** Gear ratio between the flywheel motor and the flywheel shaft. Units: none */
            public static final double FLYWHEEL_GEAR_RATIO = 50;

            /** Gear ratio between the kicker motor and the kicker shaft. Units: none */
            public static final double KICKER_GEAR_RATIO = 50;
        }

        public class Flywheel extends ConstantClass {
            /** CAN Id of the motor that drives the flywheel. */
            public static final int FLYWHEEL_MOTOR_ID = 31;

            /** Maximum current to allow the flywheel motor to draw continuously. */
            public static final int FLYWHEEL_CURRENT_LIMIT = 40;

            /**
             * Percentage of target speed to run at when at idle to avoid revving up from zero each
             * time.
             */
            public static final double FLYWHEEL_IDLE_SPEED_PERCENT = 0.6;

            /**
             * Allowed variance in the speed of the flywheel when checking if it is running
             * correctly.
             */
            public static final double FLYWHEEL_ALLOWED_SPEED_VARIANCE = 20;
        }

        public class Kicker extends ConstantClass {
            /** CAN Id of the motor that drives the kicker. */
            public static final int KICKER_MOTOR_ID = 32;

            /** Maximum current to allow the kicker motor to draw continuously. */
            public static final int KICKER_CURRENT_LIMIT = 20;

            /** The speed at which to run the kicker during normal operation. */
            public static final double KICKER_RUNNING_SPEED = 0.5;
        }
    }

    public class Hood extends ConstantClass {
        public class PhysicalCharacteristics extends ConstantClass {
            /** Gear ratio between the hood motor and the hood. Units: none */
            public static final double GEAR_RATIO = 1.5;
        }

        /** CAN Id of the motor that drives the hood. */
        public static final int HOOD_MOTOR_ID = 41;

        /** CAN Id of the encoder that reads the position of the hood. */
        public static final int HOOD_CANCODER_ID = 42;

        /** Maximum current to allow the hood motor to draw continuously. */
        public static final int HOOD_CURRENT_LIMIT = 20;

        /** Proportional value for the feedback loop control of the hood. */
        public static final double HOOD_P = 0.0;

        /** Integral value for the feedback loop control of the hood. */
        public static final double HOOD_I = 0.0;

        /** Derivative value for the feedback loop control of the hood. */
        public static final double HOOD_D = 0.0;

        /** Maximum velocity allowed for the movement of the hood. Units: m/s */
        public static final double HOOD_MAX_VELOCITY = 0.0;

        /** Maximum acceleration allowed for the movement of the hood. Units: m/s^2 */
        public static final double HOOD_MAX_ACCELERATION = 0.0;

        /** Maximum allowed angle for the hood to travel to */
        public static final double HOOD_MAX_ANGLE = 0.0;

        /**
         * Offset from the encoder measurement such that the minimum angle allowed for the hood is
         * always 0
         */
        public static final double HOOD_CANCODER_OFFSET = 0.0;

        public enum PredefinedPositions {
            /** Angle which produces a low trajectory */
            LOW_SHOT_ANGLE(0.0),
            /** Angle which produces a middle trajectory */
            MID_SHOT_ANGLE(0.0),
            /** Angle which produces a high trajectory */
            HIGH_SHOT_ANGLE(0.0);

            private final double angleValue;

            private PredefinedPositions(double angleValue) {
                this.angleValue = angleValue;
            }

            public double getValue() {
                return this.angleValue;
            }
        }
    }

    public class TestMode extends ConstantClass {
        /** Fast speed to run a motor at when running it in test mode */
        public static final double TEST_MOTOR_SPEED_FAST = 0.3;

        /** Slow speed to run a motor at when running it in test mode */
        public static final double TEST_MOTOR_SPEED_SLOW = 0.1;

        /** Short time to wait between operations in test mode */
        public static final int TEST_MODE_SHORT_WAIT_SECONDS = 1;

        /** Long time to wait between operations in test mode */
        public static final int TEST_MODE_WAIT_SECONDS = 3;
    }

    public class Simulation extends ConstantClass {
        /** Voltage of the robot to simulate with. Units: Volts */
        public static final double ROBOT_VOLTAGE = 12.0;
    }
}
