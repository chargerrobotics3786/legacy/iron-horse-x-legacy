package frc.robot.command;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystem.hood.HoodSubsytem;
import frc.robot.subsystem.intake.IntakeSubsystem;
import frc.robot.subsystem.shooter.ShooterSubsystem;

@Singleton
public class CommandService {

    private final Provider<Command> toggleIntakeCommand;
    private final Provider<Command> reverseIntakeCommand;
    private final Provider<Command> shooterToSpeedCommand;
    private final Provider<Command> shooterIdleCommand;
    private final Provider<Command> hoodToLowCommand;
    private final Provider<Command> hoodToMidCommand;
    private final Provider<Command> hoodToHighCommand;

    @Inject
    public CommandService(
            IntakeSubsystem intakeSubsystem,
            ShooterSubsystem shooterSubsystem,
            HoodSubsytem hoodSubsystem) {
        toggleIntakeCommand = intakeSubsystem::toggle;

        reverseIntakeCommand = intakeSubsystem::reverse;

        shooterToSpeedCommand = () -> shooterSubsystem.toSpeed(hoodSubsystem.atGoal());

        shooterIdleCommand = shooterSubsystem::idle;

        hoodToLowCommand = hoodSubsystem::toLow;
        hoodToMidCommand = hoodSubsystem::toMid;
        hoodToHighCommand = hoodSubsystem::toHigh;
    }

    public Command getToggleIntakeCommand() {
        return toggleIntakeCommand.get();
    }

    public Command getReverseIntakeCommand() {
        return reverseIntakeCommand.get();
    }

    public Command getShooterToSpeedCommand() {
        return shooterToSpeedCommand.get();
    }

    public Command getShooterIdleCommand() {
        return shooterIdleCommand.get();
    }

    public Command getHoodToLowCommand() {
        return hoodToLowCommand.get();
    }

    public Command getHoodToMidCommand() {
        return hoodToMidCommand.get();
    }

    public Command getHoodToHighCommand() {
        return hoodToHighCommand.get();
    }
}
