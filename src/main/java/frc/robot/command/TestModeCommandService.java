package frc.robot.command;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.util.Constants;
import frc.robot.util.SLF4JCommand;
import frc.robot.util.controlscheme.ControlSchemeOption;
import frc.robot.util.controlscheme.ControlSchemeService;
import frc.robot.util.controlscheme.TestModeScheme;
import lombok.Getter;
import org.slf4j.event.Level;

@Singleton
public class TestModeCommandService {

    @Getter private final Command testCommand;

    private class SetTestSchemeCommand extends Command {

        private final ControlSchemeService controlSchemeService;
        private final TestModeScheme testModeScheme;

        private ControlSchemeOption previousControlScheme;

        public SetTestSchemeCommand(
                ControlSchemeService controlSchemeService, TestModeScheme testModeScheme) {
            this.controlSchemeService = controlSchemeService;
            this.testModeScheme = testModeScheme;
        }

        @Override
        public void initialize() {
            previousControlScheme = controlSchemeService.setTestScheme(testModeScheme);
        }

        @Override
        public void end(boolean interrupted) {
            controlSchemeService.setControlScheme(previousControlScheme);
        }
    }

    @Inject
    public TestModeCommandService(ControlSchemeService controlSchemeService) {
        TestModeScheme testModeScheme = new TestModeScheme(0, 0);
        this.testCommand =
                new SetTestSchemeCommand(controlSchemeService, testModeScheme)
                        .raceWith(
                                getTestDrivetrainCommand(testModeScheme)
                                        .andThen(
                                                new WaitCommand(
                                                        Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                                        .andThen(getTestIntakeCommand(testModeScheme))
                                        .andThen(
                                                new WaitCommand(
                                                        Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                                        .andThen(getTestShooterCommand(testModeScheme))
                                        .andThen(
                                                new WaitCommand(
                                                        Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                                        .andThen(getTestHoodCommand(testModeScheme))
                                        .andThen(
                                                new WaitCommand(
                                                        Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                                        .andThen(
                                                new SLF4JCommand(
                                                        Level.INFO,
                                                        "Cleaning up after test mode complete...")));
    }

    /**
     * Create a test command to exercise the drivetrain in the following way:
     *
     * <ol>
     *   <li>Stop movement
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SHORT_SECONDS} seconds
     *   <li>Drive the left side at {@link Constants.TestMode#TEST_MOTOR_SPEED_FAST}
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Stop the left side and drive the right side at {@link
     *       Constants.TestMode#TEST_MOTOR_SPEED_FAST}
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Stop the right side
     * </ol>
     *
     * @return The test command sequence
     */
    private Command getTestDrivetrainCommand(TestModeScheme testModeScheme) {
        return new SLF4JCommand(Level.INFO, "Testing drivetrain...")
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_SHORT_WAIT_SECONDS))
                .andThen(
                        () ->
                                testModeScheme.setCurrentLeftDrive(
                                        Constants.TestMode.TEST_MOTOR_SPEED_FAST))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(
                        () -> {
                            testModeScheme.setCurrentLeftDrive(0);
                            testModeScheme.setCurrentRightDrive(
                                    Constants.TestMode.TEST_MOTOR_SPEED_FAST);
                        })
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setCurrentRightDrive(0));
    }

    /**
     * Creates a test command sequence which does the following in order:
     *
     * <ol>
     *   <li>Stops the intake
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SHORT_SECONDS} seconds
     *   <li>Set the intake to extend and run forward
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Set the intake to run backward
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Set the intake to retract
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Set the intake to run backward
     *   <li>Wait for {@link Constants.TestMode#TEST_MODE_WAIT_SECONDS} seconds
     *   <li>Stop the intake
     * </ol>
     *
     * @return The test command sequence
     */
    private Command getTestIntakeCommand(TestModeScheme testModeScheme) {
        return new SLF4JCommand(Level.INFO, "Testing intake...")
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_SHORT_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setIntakeShouldToggle(true))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setIntakeShouldReverse(true))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(
                        () -> {
                            testModeScheme.setIntakeShouldReverse(false);
                            testModeScheme.setIntakeShouldToggle(true);
                        })
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setIntakeShouldReverse(true))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setIntakeShouldReverse(false));
    }

    /**
     * Creates a test command sequence which does the following in order:
     *
     * <ol>
     *   <li>Stop the shooter
     *   <li>Wait for 1 second
     *   <li>Tell the shooter to fire
     *   <li>Wait for 3 seconds
     *   <li>Tell the shooter to stop firing
     *   <li>Wait for 3 seconds
     * </ol>
     *
     * @return The test command sequence
     */
    private Command getTestShooterCommand(TestModeScheme testModeScheme) {
        return new SLF4JCommand(Level.INFO, "Testing Shooter...")
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_SHORT_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setShooterShouldFire(true))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS))
                .andThen(() -> testModeScheme.setShooterShouldFire(false))
                .andThen(new WaitCommand(Constants.TestMode.TEST_MODE_WAIT_SECONDS));
    }

    public Command getTestHoodCommand(TestModeScheme testModeScheme) {
        // TODO: Define the control scheme for the hood in order to set this test mode up
        return new SLF4JCommand(Level.INFO, "Testing Hood...");
    }
}
