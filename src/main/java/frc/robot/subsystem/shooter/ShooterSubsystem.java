// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystem.shooter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.subsystem.shooter.io.ShooterIO;
import frc.robot.subsystem.shooter.io.ShooterIOInputsAutoLogged;
import frc.robot.util.Constants;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.io.IOSubsystem;
import frc.robot.util.io.IOType;
import java.util.List;
import java.util.Map;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

@EqualsAndHashCode(callSuper = true)
@Singleton
public class ShooterSubsystem extends IOSubsystem<ShooterIO> {

    public enum FlywheelState {
        IDLE,
        SPINNING_UP,
        SPINNING_DOWN,
        AT_SPEED
    }

    public enum KickerState {
        IDLE,
        FEEDING
    }

    private ShooterIOInputsAutoLogged inputs;

    @AutoLogOutput private FlywheelState currentFlywheelState;
    @AutoLogOutput private KickerState currentKickerState;

    @AutoLogOutput private FlywheelState desiredFlywheelState;
    @AutoLogOutput private KickerState desiredKickerState;
    @Getter @Setter @AutoLogOutput private double desiredSpeed;

    private final FeatureFlagStore featureFlagStore;

    /** Creates a new ShooterSubsystem. */
    @Inject
    public ShooterSubsystem(
            IOType ioType, Map<IOType, ShooterIO> shooterIO, FeatureFlagStore featureFlagStore) {
        super(shooterIO.get(ioType));

        inputs = new ShooterIOInputsAutoLogged();

        currentFlywheelState = FlywheelState.IDLE;
        currentKickerState = KickerState.IDLE;

        this.featureFlagStore = featureFlagStore;
    }

    public Command idle() {
        Command idleCommand =
                flywheelStateUpdate(FlywheelState.IDLE)
                        .alongWith(kickerStateUpdate(KickerState.IDLE));
        idleCommand.addRequirements(this);
        return idleCommand;
    }

    public Command toSpeed(Trigger feedTrigger) {
        return flywheelStateUpdate(FlywheelState.AT_SPEED)
                .until(feedTrigger)
                .andThen(kickerStateUpdate(KickerState.FEEDING));
    }

    private Command flywheelStateUpdate(FlywheelState state) {
        return new InstantCommand(() -> this.desiredFlywheelState = state);
    }

    public FlywheelState getDesiredFlywheelState() {
        return this.desiredFlywheelState;
    }

    public FlywheelState getCurrentFlywheelState() {
        return this.currentFlywheelState;
    }

    public Trigger flywheelAtState(FlywheelState flywheelState) {
        return new Trigger(() -> this.currentFlywheelState == flywheelState);
    }

    public Trigger flywheelInStates(List<FlywheelState> flywheelStates) {
        return new Trigger(() -> flywheelStates.contains(this.currentFlywheelState));
    }

    private Command kickerStateUpdate(KickerState state) {
        return new InstantCommand(() -> this.desiredKickerState = state);
    }

    public KickerState getDesiredKickerState() {
        return this.desiredKickerState;
    }

    public KickerState getCurrentKickerState() {
        return this.currentKickerState;
    }

    public Trigger kickerAtState(KickerState kickerState) {
        return new Trigger(() -> this.currentKickerState == kickerState);
    }

    public Trigger kickerInStates(List<KickerState> kickerStates) {
        return new Trigger(() -> kickerStates.contains(this.currentKickerState));
    }

    private boolean speedWithinVariance(double checkWithin) {
        return Math.abs((inputs.getFlywheelAppliedVolts() / 12.0) - checkWithin)
                <= Constants.Shooter.Flywheel.FLYWHEEL_ALLOWED_SPEED_VARIANCE;
    }

    @Override
    public void periodic() {
        this.subsystemIO.updateInputs(inputs);
        Logger.processInputs("Shooter", inputs);

        updateCurrentState();

        operateOnCurrentState();
    }

    private void updateCurrentState() {
        // Get current flywheel state
        if (speedWithinVariance(desiredSpeed)) {
            currentFlywheelState = FlywheelState.AT_SPEED;
        } else if (speedWithinVariance(
                desiredSpeed * Constants.Shooter.Flywheel.FLYWHEEL_IDLE_SPEED_PERCENT)) {
            currentFlywheelState = FlywheelState.IDLE;
        } else {
            currentFlywheelState =
                    desiredFlywheelState == FlywheelState.IDLE
                            ? FlywheelState.SPINNING_DOWN
                            : FlywheelState.SPINNING_UP;
        }
    }

    private void operateOnCurrentState() {
        if (featureFlagStore.isEnabled(FeatureFlag.SHOOTER_ENABLED).orElse(false)) {
            // Update Kicker State if relevant
            if (desiredKickerState == KickerState.FEEDING
                    && currentFlywheelState == FlywheelState.AT_SPEED) {
                subsystemIO.setKickerValue(Constants.Shooter.Kicker.KICKER_RUNNING_SPEED);
                currentKickerState = KickerState.FEEDING;
            } else {
                subsystemIO.setKickerValue(0);
                currentKickerState = KickerState.IDLE;
            }

            if (currentFlywheelState != desiredFlywheelState) {
                subsystemIO.setFlywheelValue(
                        desiredFlywheelState == FlywheelState.IDLE
                                ? desiredSpeed
                                        * Constants.Shooter.Flywheel.FLYWHEEL_IDLE_SPEED_PERCENT
                                : desiredSpeed);
            }
        } else {
            subsystemIO.setKickerValue(0.0);
            subsystemIO.setFlywheelValue(0.0);
        }
    }
}
