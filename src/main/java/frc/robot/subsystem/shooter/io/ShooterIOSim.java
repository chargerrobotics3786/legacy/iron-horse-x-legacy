package frc.robot.subsystem.shooter.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import frc.robot.util.Constants;
import frc.robot.util.modules.MetadataModule.PeriodProvider;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class ShooterIOSim extends ShooterIO {

    private FlywheelSim flywheelSim;
    private FlywheelSim kickerSim;

    private double currentFlywheelValue;
    private double currentKickerValue;

    private final PeriodProvider periodProvider;

    @Inject
    public ShooterIOSim(SystemToggleStore store, PeriodProvider periodProvider) {
        super(store);
        this.periodProvider = periodProvider;
    }

    protected void initialize() {
        this.flywheelSim =
                new FlywheelSim(
                        DCMotor.getNEO(1),
                        Constants.Shooter.PhysicalCharacteristics.FLYWHEEL_GEAR_RATIO,
                        0.003547);
        this.kickerSim =
                new FlywheelSim(
                        DCMotor.getNeo550(1),
                        Constants.Shooter.PhysicalCharacteristics.KICKER_GEAR_RATIO,
                        0.01);

        this.currentFlywheelValue = 0.0;
        this.currentKickerValue = 0.0;
    }

    @Override
    protected void updateInputsInternal(ShooterIOInputs inputs) {
        flywheelSim.update(periodProvider.getPeriod());
        kickerSim.update(periodProvider.getPeriod());
        inputs.setFlywheelPositionRad(
                flywheelSim.getAngularVelocityRadPerSec() * periodProvider.getPeriod());
        inputs.setFlywheelAppliedVolts(currentFlywheelValue * Constants.Simulation.ROBOT_VOLTAGE);
        inputs.setFlywheelCurrentAmps(flywheelSim.getCurrentDrawAmps());
        inputs.setFlywheelVelocityRadPerSec(flywheelSim.getAngularVelocityRadPerSec());
        inputs.setKickerPositionRad(
                kickerSim.getAngularVelocityRadPerSec() * periodProvider.getPeriod());
        inputs.setKickerAppliedVolts(currentKickerValue * Constants.Simulation.ROBOT_VOLTAGE);
        inputs.setKickerCurrentAmps(kickerSim.getCurrentDrawAmps());
        inputs.setKickerVelocityRadPerSec(kickerSim.getAngularVelocityRadPerSec());
    }

    @Override
    protected void setFlywheelValueInternal(double value) {
        this.currentFlywheelValue = value;
        this.flywheelSim.setInputVoltage(value * Constants.Simulation.ROBOT_VOLTAGE);
    }

    @Override
    protected void setKickerValueInternal(double value) {
        this.currentKickerValue = value;
        this.kickerSim.setInputVoltage(value * Constants.Simulation.ROBOT_VOLTAGE);
    }
}
