package frc.robot.subsystem.shooter.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class ShooterIOReplay extends ShooterIO {

    @Inject
    public ShooterIOReplay(SystemToggleStore store) {
        super(store);
    }

    @Override
    protected void updateInputsInternal(ShooterIOInputs inputs) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void setFlywheelValueInternal(double value) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void setKickerValueInternal(double value) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void initialize() {
        // Replay implementations contain no actual implementation
    }
}
