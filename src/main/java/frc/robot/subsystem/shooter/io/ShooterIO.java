package frc.robot.subsystem.shooter.io;

import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import frc.robot.util.systemtoggle.ToggleableIO;
import lombok.Data;
import org.littletonrobotics.junction.AutoLog;

public abstract class ShooterIO extends ToggleableIO {
    private static final SystemToggle TOGGLE = SystemToggle.SHOOTER;

    protected ShooterIO(SystemToggleStore store) {
        super(TOGGLE, store);
    }

    @AutoLog
    @Data
    public static class ShooterIOInputs {
        // Flywheel motor inputs
        /** The current position of the flywheel motor in radians. */
        protected double flywheelPositionRad = 0.0;

        /** The velocity of the flywheel motor in radians per second. */
        protected double flywheelVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the flywheel motor in Volts. */
        protected double flywheelAppliedVolts = 0.0;

        /** The current amperage being pulled by the flywheel motor in Amperes. */
        protected double flywheelCurrentAmps = 0.0;

        // Kicker motor inputs
        /** The current position of the kicker motor in radians. */
        protected double kickerPositionRad = 0.0;

        /** The velocity of the kicker motor in radians per second. */
        protected double kickerVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the kicker motor in Volts. */
        protected double kickerAppliedVolts = 0.0;

        /** The current amperage being pulled by the kicker motor in Amperes. */
        protected double kickerCurrentAmps = 0.0;
    }

    /**
     * Updates the {@link ShooterIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    protected abstract void updateInputsInternal(ShooterIOInputs inputs);

    /**
     * Set the desired percent speed to run the flywheel motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    protected abstract void setFlywheelValueInternal(double value);

    /**
     * Set the desired percent speed to run the kicker motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    protected abstract void setKickerValueInternal(double value);

    /**
     * Updates the {@link ShooterIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    public void updateInputs(ShooterIOInputs inputs) {
        if (isToggledOn()) {
            updateInputsInternal(inputs);
        }
    }

    /**
     * Set the desired percent speed to run the flywheel motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    public void setFlywheelValue(double value) {
        if (isToggledOn()) {
            setFlywheelValueInternal(value);
        }
    }

    /**
     * Set the desired percent speed to run the kicker motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    public void setKickerValue(double value) {
        if (isToggledOn()) {
            setKickerValueInternal(value);
        }
    }
}
