package frc.robot.subsystem.shooter.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.util.Units;
import frc.robot.util.Constants;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class ShooterIOSparkMax extends ShooterIO {

    private CANSparkMax flywheelMotor;
    private CANSparkMax kickerMotor;

    private RelativeEncoder flywheelEncoder;
    private RelativeEncoder kickerEncoder;

    @Inject
    public ShooterIOSparkMax(SystemToggleStore store) {
        super(store);
    }

    protected void initialize() {
        flywheelMotor =
                new CANSparkMax(Constants.Shooter.Flywheel.FLYWHEEL_MOTOR_ID, MotorType.kBrushless);
        flywheelMotor.setIdleMode(IdleMode.kCoast);
        flywheelMotor.setSmartCurrentLimit(Constants.Shooter.Flywheel.FLYWHEEL_CURRENT_LIMIT);
        flywheelEncoder = flywheelMotor.getEncoder();

        kickerMotor =
                new CANSparkMax(Constants.Shooter.Kicker.KICKER_MOTOR_ID, MotorType.kBrushless);
        kickerMotor.setIdleMode(IdleMode.kCoast);
        kickerMotor.setSmartCurrentLimit(Constants.Shooter.Kicker.KICKER_CURRENT_LIMIT);
        kickerEncoder = kickerMotor.getEncoder();
    }

    @Override
    protected void updateInputsInternal(ShooterIOInputs inputs) {
        inputs.setFlywheelPositionRad(
                Units.rotationsToRadians(
                        flywheelEncoder.getPosition()
                                / Constants.Shooter.PhysicalCharacteristics.FLYWHEEL_GEAR_RATIO));
        inputs.setFlywheelVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        flywheelEncoder.getVelocity()
                                / Constants.Shooter.PhysicalCharacteristics.FLYWHEEL_GEAR_RATIO));
        inputs.setFlywheelAppliedVolts(
                flywheelMotor.getAppliedOutput() * flywheelMotor.getBusVoltage());
        inputs.setFlywheelCurrentAmps(flywheelMotor.getOutputCurrent());

        inputs.setKickerPositionRad(
                Units.rotationsToRadians(
                        kickerEncoder.getPosition()
                                / Constants.Shooter.PhysicalCharacteristics.KICKER_GEAR_RATIO));
        inputs.setKickerVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        kickerEncoder.getVelocity()
                                / Constants.Shooter.PhysicalCharacteristics.KICKER_GEAR_RATIO));
        inputs.setKickerAppliedVolts(kickerMotor.getAppliedOutput() * kickerMotor.getBusVoltage());
        inputs.setKickerCurrentAmps(kickerMotor.getOutputCurrent());
    }

    @Override
    protected void setFlywheelValueInternal(double value) {
        flywheelMotor.set(MathUtil.clamp(value, -1, 1));
    }

    @Override
    protected void setKickerValueInternal(double value) {
        kickerMotor.set(MathUtil.clamp(value, -1, 1));
    }
}
