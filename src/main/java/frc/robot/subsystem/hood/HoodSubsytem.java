package frc.robot.subsystem.hood;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.subsystem.hood.io.HoodIO;
import frc.robot.subsystem.hood.io.HoodIOInputsAutoLogged;
import frc.robot.util.Constants;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.io.IOType;
import frc.robot.util.io.ProfiledPIDIOSubsystem;
import java.util.Map;
import lombok.EqualsAndHashCode;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

/** Subsystem to run the robot's Shooter Hood and maintain its position */
@EqualsAndHashCode(callSuper = true)
@Singleton
public class HoodSubsytem extends ProfiledPIDIOSubsystem<HoodIO> {
    private FeatureFlagStore featureFlagStore;

    private final HoodIOInputsAutoLogged inputs;

    /**
     * @param io The {@link HoodIO} to use when interfacing with hardware
     */
    @Inject
    public HoodSubsytem(IOType ioType, Map<IOType, HoodIO> io, FeatureFlagStore featureFlagStore) {
        super(
                // The ProfiledPIDController used by the subsystem
                new ProfiledPIDController(
                        Constants.Hood.HOOD_P,
                        Constants.Hood.HOOD_I,
                        Constants.Hood.HOOD_D,
                        // The motion profile constraints
                        new TrapezoidProfile.Constraints(
                                Constants.Hood.HOOD_MAX_VELOCITY,
                                Constants.Hood.HOOD_MAX_ACCELERATION)),
                io.get(ioType));
        this.featureFlagStore = featureFlagStore;
        inputs = new HoodIOInputsAutoLogged();
    }

    @AutoLogOutput
    private double getCurrentGoal() {
        return super.getController().getGoal().position;
    }

    public Command toLow() {
        return new InstantCommand(
                () -> this.set(Constants.Hood.PredefinedPositions.LOW_SHOT_ANGLE.getValue()));
    }

    public Command toMid() {
        return new InstantCommand(
                () -> this.set(Constants.Hood.PredefinedPositions.MID_SHOT_ANGLE.getValue()));
    }

    public Command toHigh() {
        return new InstantCommand(
                () -> this.set(Constants.Hood.PredefinedPositions.HIGH_SHOT_ANGLE.getValue()));
    }

    public void set(double goal) {
        // Limit the goal to stay within acceptable values
        if (goal < 0) {
            goal = 0;
        }
        if (goal > Constants.Hood.HOOD_MAX_ANGLE) {
            goal = Constants.Hood.HOOD_MAX_ANGLE;
        }
        super.setGoal(goal);
    }

    @Override
    public void useOutput(double output, TrapezoidProfile.State setpoint) {
        if (featureFlagStore.isEnabled(FeatureFlag.HOOD_ENABLED).orElse(false)) {
            subsystemIO.setMotorInput(output);
        } else {
            subsystemIO.setMotorInput(0);
        }
    }

    @Override
    public double getMeasurement() {
        return inputs.getHoodReferencePosition() - Constants.Hood.HOOD_CANCODER_OFFSET;
    }

    public Trigger atGoal() {
        return new Trigger(() -> this.getController().atGoal());
    }

    @Override
    public void periodic() {
        subsystemIO.updateInputs(inputs);
        Logger.processInputs("Hood", inputs);

        super.periodic();
    }
}
