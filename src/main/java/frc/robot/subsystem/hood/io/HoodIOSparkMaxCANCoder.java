package frc.robot.subsystem.hood.io;

import com.ctre.phoenix6.StatusSignal;
import com.ctre.phoenix6.configs.CANcoderConfiguration;
import com.ctre.phoenix6.hardware.CANcoder;
import com.ctre.phoenix6.signals.AbsoluteSensorRangeValue;
import com.ctre.phoenix6.signals.SensorDirectionValue;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.math.util.Units;
import frc.robot.util.Constants;
import frc.robot.util.systemtoggle.SystemToggleStore;

/**
 * Implementation of {@link HoodIO} using a {@link CANSparkMax} motor controller and {@link
 * CANCoder}
 */
@Singleton
public class HoodIOSparkMaxCANCoder extends HoodIO {
    private CANcoder canCoder;
    private StatusSignal<Double> absolutePositionSignal;
    private CANSparkMax hoodMotor;

    private RelativeEncoder hoodEncoder;

    @Inject
    public HoodIOSparkMaxCANCoder(SystemToggleStore store) {
        super(store);
    }

    protected void initialize() {
        this.canCoder = new CANcoder(Constants.Hood.HOOD_CANCODER_ID);
        CANcoderConfiguration configuration = new CANcoderConfiguration();
        configuration.MagnetSensor.AbsoluteSensorRange =
                AbsoluteSensorRangeValue.Signed_PlusMinusHalf;
        configuration.MagnetSensor.SensorDirection = SensorDirectionValue.CounterClockwise_Positive;
        configuration.MagnetSensor.MagnetOffset = Constants.Hood.HOOD_CANCODER_OFFSET;
        this.canCoder.getConfigurator().apply(configuration);
        absolutePositionSignal = canCoder.getAbsolutePosition();

        this.hoodMotor = new CANSparkMax(Constants.Hood.HOOD_MOTOR_ID, MotorType.kBrushless);

        this.hoodEncoder = hoodMotor.getEncoder();

        this.hoodMotor.setIdleMode(IdleMode.kCoast);
        this.hoodMotor.setSmartCurrentLimit(Constants.Hood.HOOD_CURRENT_LIMIT);

        this.canCoder.setPosition(Constants.Hood.HOOD_CANCODER_OFFSET);
    }

    @Override
    protected void updateInputsInternal(HoodIOInputs inputs) {
        inputs.setHoodReferencePosition(absolutePositionSignal.refresh().getValueAsDouble());

        inputs.setHoodVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        hoodEncoder.getVelocity()
                                / Constants.Hood.PhysicalCharacteristics.GEAR_RATIO));
        inputs.setHoodAppliedVolts(hoodMotor.getAppliedOutput() * hoodMotor.getBusVoltage());
        inputs.setHoodCurrentAmps(hoodMotor.getOutputCurrent());
    }

    @Override
    protected void setMotorInputInternal(double input) {
        hoodMotor.set(input);
    }
}
