package frc.robot.subsystem.hood.io;

import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import frc.robot.util.systemtoggle.ToggleableIO;
import lombok.Data;
import org.littletonrobotics.junction.AutoLog;

/** Abstract class representing the hardware I/O for the Hood */
public abstract class HoodIO extends ToggleableIO {
    private static final SystemToggle TOGGLE = SystemToggle.HOOD;

    protected HoodIO(SystemToggleStore store) {
        super(TOGGLE, store);
    }

    @AutoLog
    @Data
    public static class HoodIOInputs {
        /** The position in rotations [0, 1) */
        protected double hoodReferencePosition = 0.0;

        // Hood motor inputs

        /** The velocity of the motor in radians per second. */
        protected double hoodVelocityRadPerSec = 0.0;

        /** The voltage being applied to the motor in Volts. */
        protected double hoodAppliedVolts = 0.0;

        /** The current draw of the motor in Amperes. */
        protected double hoodCurrentAmps = 0.0;
    }

    /**
     * Updates the {@link HoodIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    public void updateInputs(HoodIOInputs inputs) {
        if (isToggledOn()) {
            updateInputsInternal(inputs);
        }
    }

    /**
     * Updates the {@link HoodIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    protected abstract void updateInputsInternal(HoodIOInputs inputs);

    /**
     * Instruct the hood motor to run at a certain percentage
     *
     * @param input The percentage speed to run the motor at in (-1,1)
     */
    public void setMotorInput(double input) {
        if (isToggledOn()) {
            setMotorInput(input);
        }
    }

    /**
     * Instruct the hood motor to run at a certain percentage
     *
     * @param input The percentage speed to run the motor at in (-1,1)
     */
    protected abstract void setMotorInputInternal(double input);
}
