package frc.robot.subsystem.hood.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import frc.robot.util.Constants;
import frc.robot.util.modules.MetadataModule.PeriodProvider;
import frc.robot.util.systemtoggle.SystemToggleStore;

/** Implementation of {@link HoodIO} used when running in simulation. */
@Singleton
public class HoodIOSim extends HoodIO {

    private FlywheelSim hoodSim;

    private double currentHoodValue;

    private final PeriodProvider periodProvider;

    @Inject
    public HoodIOSim(SystemToggleStore store, PeriodProvider periodProvider) {
        super(store);
        this.periodProvider = periodProvider;
    }

    protected void initialize() {
        this.hoodSim =
                new FlywheelSim(
                        DCMotor.getNEO(1), Constants.Hood.PhysicalCharacteristics.GEAR_RATIO, 0.01);
        this.currentHoodValue = 0.0;
    }

    @Override
    protected void updateInputsInternal(HoodIOInputs inputs) {
        hoodSim.update(periodProvider.getPeriod());
        inputs.setHoodReferencePosition(
                hoodSim.getAngularVelocityRadPerSec() * periodProvider.getPeriod());
        inputs.setHoodAppliedVolts(currentHoodValue * Constants.Simulation.ROBOT_VOLTAGE);
        inputs.setHoodCurrentAmps(hoodSim.getCurrentDrawAmps());
        inputs.setHoodVelocityRadPerSec(hoodSim.getAngularVelocityRadPerSec());
    }

    @Override
    protected void setMotorInputInternal(double input) {
        this.currentHoodValue = input;
        hoodSim.setInputVoltage(input * Constants.Simulation.ROBOT_VOLTAGE);
    }
}
