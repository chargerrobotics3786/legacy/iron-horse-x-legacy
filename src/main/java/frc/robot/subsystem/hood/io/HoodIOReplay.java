package frc.robot.subsystem.hood.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import frc.robot.util.systemtoggle.SystemToggleStore;

/** Implementation of {@link HoodIO} used in replaying logs. */
@Singleton
public class HoodIOReplay extends HoodIO {

    @Inject
    public HoodIOReplay(SystemToggleStore store) {
        super(store);
    }

    @Override
    protected void updateInputsInternal(HoodIOInputs inputs) {
        // Replay implementations are no-op implementations
    }

    @Override
    protected void setMotorInputInternal(double input) {
        // Replay implementations are no-op implementations
    }

    @Override
    protected void initialize() {
        // Replay implementations are no-op implementations
    }
}
