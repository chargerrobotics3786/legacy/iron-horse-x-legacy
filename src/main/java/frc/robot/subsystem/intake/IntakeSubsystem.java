package frc.robot.subsystem.intake;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.subsystem.intake.io.IntakeIO;
import frc.robot.subsystem.intake.io.IntakeIOInputsAutoLogged;
import frc.robot.util.Constants;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.io.IOSubsystem;
import frc.robot.util.io.IOType;
import java.util.List;
import java.util.Map;
import lombok.EqualsAndHashCode;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;

/** Subsystem to run the robot's Intake system to collect, manipulate, and eject game pieces */
@EqualsAndHashCode(callSuper = true)
@Singleton
public class IntakeSubsystem extends IOSubsystem<IntakeIO> {

    /** Enum defining the possible states that belts for the collector and transition can run in. */
    public enum BeltDirection {
        OFF,
        INWARD,
        OUTWARD
    }

    /** Enum defining all of the possible states that the intake can be in. */
    public enum IntakeState {
        RETRACTED(false, BeltDirection.OFF),
        RETRACTED_RUNNING(false, BeltDirection.INWARD),
        RETRACTED_REVERSING(false, BeltDirection.OUTWARD),
        EXTENDED(true, BeltDirection.OFF),
        EXTENDED_RUNNING(true, BeltDirection.INWARD),
        EXTENDED_REVERSING(true, BeltDirection.OUTWARD);

        private boolean isExtended;
        private BeltDirection beltDirection;

        private IntakeState(boolean isExtended, BeltDirection beltDirection) {
            this.isExtended = isExtended;
            this.beltDirection = beltDirection;
        }

        /**
         * @return Whether or not this state includes the collector being extended.
         */
        public boolean isExtended() {
            return this.isExtended;
        }

        /**
         * @return Which direction the belts should be running in this state.
         */
        public BeltDirection getBeltDirection() {
            return this.beltDirection;
        }
    }

    private IntakeIOInputsAutoLogged inputs;

    @AutoLogOutput private IntakeState desiredState;
    @AutoLogOutput private IntakeState currentState;

    private final FeatureFlagStore featureFlagStore;

    /**
     * @param intakeIO The {@link IntakeIO} hardware representation to use
     */
    @Inject
    public IntakeSubsystem(
            IOType ioType, Map<IOType, IntakeIO> intakeIO, FeatureFlagStore featureFlagStore) {
        super(intakeIO.get(ioType));
        this.inputs = new IntakeIOInputsAutoLogged();
        this.currentState = determineStartingState();
        this.featureFlagStore = featureFlagStore;
    }

    /**
     * @return The identified starting state of the Intake on boot
     */
    private IntakeState determineStartingState() {
        subsystemIO.setCollectorValue(0);
        subsystemIO.setTransitionValue(0);
        if (inputs.isReedSwitchClosed()) {
            subsystemIO.setSolenoidValue(Value.kForward);
            return IntakeState.EXTENDED;
        }
        subsystemIO.setSolenoidValue(Value.kReverse);
        return IntakeState.RETRACTED;
    }

    public Command reverse() {
        return new InstantCommand(
                () -> {
                    switch (currentState) {
                        case RETRACTED, RETRACTED_RUNNING ->
                                setDesiredState(IntakeState.RETRACTED_REVERSING);
                        case RETRACTED_REVERSING -> setDesiredState(IntakeState.RETRACTED);
                        case EXTENDED, EXTENDED_RUNNING ->
                                setDesiredState(IntakeState.EXTENDED_REVERSING);
                        case EXTENDED_REVERSING -> setDesiredState(IntakeState.EXTENDED);
                    }
                });
    }

    public Command toggle() {
        return new InstantCommand(
                () -> {
                    switch (currentState) {
                        case RETRACTED, RETRACTED_RUNNING, RETRACTED_REVERSING ->
                                setDesiredState(IntakeState.EXTENDED_RUNNING);
                        case EXTENDED, EXTENDED_RUNNING, EXTENDED_REVERSING ->
                                setDesiredState(IntakeState.RETRACTED);
                    }
                });
    }

    @VisibleForTesting
    public void setDesiredState(IntakeState intakeState) {
        this.desiredState = intakeState;
    }

    /**
     * @return The current state of the intake, regardless of the desired state
     */
    public IntakeState getCurrentState() {
        return this.currentState;
    }

    private void toggleExtension() {
        subsystemIO.toggleSolenoidValue();
    }

    private void setBeltDirection(BeltDirection beltDirection) {
        double speedMultiplier =
                switch (beltDirection) {
                    case OFF -> 0.0;
                    case INWARD -> 1;
                    case OUTWARD -> -1;
                };
        subsystemIO.setCollectorValue(speedMultiplier * Constants.Intake.COLLECTOR_BELT_SPEED);
        subsystemIO.setTransitionValue(speedMultiplier * Constants.Intake.TRANSITION_BELT_SPEED);
    }

    public Trigger atState(IntakeState intakeState) {
        return new Trigger(() -> this.currentState == intakeState);
    }

    public Trigger inStates(List<IntakeState> intakeStates) {
        return new Trigger(() -> intakeStates.contains(this.currentState));
    }

    @Override
    public void periodic() {
        this.subsystemIO.updateInputs(inputs);
        Logger.processInputs("Intake", inputs);
        // Update the current state
        if (inputs.isReedSwitchClosed()) {
            handleStateUpdate(
                    IntakeState.EXTENDED,
                    IntakeState.EXTENDED_RUNNING,
                    IntakeState.EXTENDED_REVERSING);
        } else {
            handleStateUpdate(
                    IntakeState.RETRACTED,
                    IntakeState.RETRACTED_RUNNING,
                    IntakeState.RETRACTED_REVERSING);
        }
        // Attempt to reach desired state if not already there.
        if (featureFlagStore.isEnabled(FeatureFlag.INTAKE_ENABLED).orElse(false)) {
            if (desiredState != currentState) {
                if (desiredState.isExtended() != currentState.isExtended()) {
                    this.toggleExtension();
                }
                if (desiredState.getBeltDirection() != currentState.getBeltDirection()) {
                    this.setBeltDirection(desiredState.beltDirection);
                }
            }
        } else {
            this.subsystemIO.setCollectorValue(0.0);
            this.subsystemIO.setTransitionValue(0.0);
            this.subsystemIO.setSolenoidValue(Value.kOff);
        }
    }

    private void handleStateUpdate(
            IntakeState idleState, IntakeState runningState, IntakeState reversingState) {
        if (Math.abs(inputs.getCollectorAppliedVolts()) - Constants.Intake.BELT_SPEED_DEAD_ZONE
                < 0) {
            currentState = idleState;
        } else if (inputs.getCollectorAppliedVolts() > 0) {
            currentState = runningState;
        } else {
            currentState = reversingState;
        }
    }
}
