package frc.robot.subsystem.intake.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.simulation.FlywheelSim;
import frc.robot.util.Constants;
import frc.robot.util.modules.MetadataModule.PeriodProvider;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class IntakeIOSim extends IntakeIO {

    private FlywheelSim collectorSim;
    private FlywheelSim transitionSim;

    private double currentCollectorValue;
    private double currentTransitionValue;

    private boolean reedSwitchValue;

    private final PeriodProvider periodProvider;

    @Inject
    public IntakeIOSim(SystemToggleStore store, PeriodProvider periodProvider) {
        super(store);
        this.periodProvider = periodProvider;
    }

    protected void initialize() {
        this.collectorSim =
                new FlywheelSim(
                        DCMotor.getNEO(1),
                        Constants.Intake.PhysicalCharacteristics.COLLECTOR_GEAR_RATIO,
                        0.01);
        this.transitionSim =
                new FlywheelSim(
                        DCMotor.getNEO(1),
                        Constants.Intake.PhysicalCharacteristics.TRANSITION_GEAR_RATIO,
                        0.01);
        this.currentCollectorValue = 0.0;
        this.currentTransitionValue = 0.0;
        this.reedSwitchValue = false;
    }

    @Override
    protected void updateInputsInternal(IntakeIOInputs inputs) {
        collectorSim.update(periodProvider.getPeriod());
        transitionSim.update(periodProvider.getPeriod());
        inputs.setCollectorPositionRad(
                collectorSim.getAngularVelocityRadPerSec() * periodProvider.getPeriod());
        inputs.setCollectorAppliedVolts(currentCollectorValue * Constants.Simulation.ROBOT_VOLTAGE);
        inputs.setCollectorCurrentAmps(collectorSim.getCurrentDrawAmps());
        inputs.setCollectorVelocityRadPerSec(collectorSim.getAngularVelocityRadPerSec());
        inputs.setTransitionPositionRad(
                transitionSim.getAngularVelocityRadPerSec() * periodProvider.getPeriod());
        inputs.setTransitionAppliedVolts(
                currentTransitionValue * Constants.Simulation.ROBOT_VOLTAGE);
        inputs.setTransitionCurrentAmps(transitionSim.getCurrentDrawAmps());
        inputs.setTransitionVelocityRadPerSec(transitionSim.getAngularVelocityRadPerSec());
    }

    @Override
    protected void setSolenoidValueInternal(Value value) {
        this.reedSwitchValue = value.equals(Value.kForward);
    }

    @Override
    protected void toggleSolenoidValueInternal() {
        this.reedSwitchValue = !this.reedSwitchValue;
    }

    @Override
    protected void setCollectorValueInternal(double value) {
        this.currentCollectorValue = value;
        this.collectorSim.setInputVoltage(value * Constants.Simulation.ROBOT_VOLTAGE);
    }

    @Override
    protected void setTransitionValueInternal(double value) {
        this.currentTransitionValue = value;
        this.transitionSim.setInputVoltage(value * Constants.Simulation.ROBOT_VOLTAGE);
    }
}
