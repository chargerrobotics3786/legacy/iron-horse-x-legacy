package frc.robot.subsystem.intake.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class IntakeIOReplay extends IntakeIO {

    @Inject
    public IntakeIOReplay(SystemToggleStore store) {
        super(store);
    }

    @Override
    protected void updateInputsInternal(IntakeIOInputs inputs) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void setSolenoidValueInternal(Value value) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void toggleSolenoidValueInternal() {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void setCollectorValueInternal(double value) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void setTransitionValueInternal(double value) {
        // Replay implementations contain no actual implementation
    }

    @Override
    protected void initialize() {
        // Replay implementations contain no actual implementation
    }
}
