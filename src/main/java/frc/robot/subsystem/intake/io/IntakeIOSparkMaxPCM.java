package frc.robot.subsystem.intake.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import frc.robot.util.Constants;
import frc.robot.util.systemtoggle.SystemToggleStore;

@Singleton
public class IntakeIOSparkMaxPCM extends IntakeIO {

    private DoubleSolenoid intakeDoubleSolenoid;
    private DigitalInput reedSwitchInput;

    private CANSparkMax collectorMotor;
    private CANSparkMax transitionMotor;

    private RelativeEncoder collectorEncoder;
    private RelativeEncoder transitionEncoder;

    @Inject
    public IntakeIOSparkMaxPCM(SystemToggleStore store) {
        super(store);
    }

    protected void initialize() {
        intakeDoubleSolenoid =
                new DoubleSolenoid(
                        PneumaticsModuleType.CTREPCM,
                        Constants.Intake.FORWARD_PNEUMATICS_CHANNEL,
                        Constants.Intake.REVERSE_PNEUMATICS_CHANNEL);

        collectorMotor =
                new CANSparkMax(Constants.Intake.COLLECTOR_BELT_MOTOR_ID, MotorType.kBrushless);
        collectorMotor.setIdleMode(IdleMode.kCoast);
        collectorMotor.setSmartCurrentLimit(Constants.Intake.COLLECTOR_CURRENT_LIMIT_AMPS);
        collectorEncoder = collectorMotor.getEncoder();

        transitionMotor =
                new CANSparkMax(Constants.Intake.TRANSITION_BELT_MOTOR_ID, MotorType.kBrushless);
        transitionMotor.setIdleMode(IdleMode.kCoast);
        transitionMotor.setSmartCurrentLimit(Constants.Intake.TRANSITION_CURRENT_LIMIT_AMPS);
        transitionEncoder = transitionMotor.getEncoder();

        reedSwitchInput = new DigitalInput(Constants.Intake.REED_SWITCH_DIGITAL_CHANNEL);
    }

    @Override
    protected void updateInputsInternal(IntakeIOInputs inputs) {
        inputs.setSolenoidValue(intakeDoubleSolenoid.get());
        inputs.setReedSwitchClosed(reedSwitchInput.get());

        inputs.setCollectorPositionRad(
                Units.rotationsToRadians(
                        collectorEncoder.getPosition()
                                / Constants.Intake.PhysicalCharacteristics.COLLECTOR_GEAR_RATIO));
        inputs.setCollectorVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        collectorEncoder.getVelocity()
                                / Constants.Intake.PhysicalCharacteristics.COLLECTOR_GEAR_RATIO));
        inputs.setCollectorAppliedVolts(
                collectorMotor.getAppliedOutput() * collectorMotor.getBusVoltage());
        inputs.setCollectorCurrentAmps(collectorMotor.getOutputCurrent());

        inputs.setTransitionPositionRad(
                Units.rotationsToRadians(
                        transitionEncoder.getPosition()
                                / Constants.Intake.PhysicalCharacteristics.TRANSITION_GEAR_RATIO));
        inputs.setTransitionVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        transitionEncoder.getVelocity()
                                / Constants.Intake.PhysicalCharacteristics.TRANSITION_GEAR_RATIO));
        inputs.setTransitionAppliedVolts(
                transitionMotor.getAppliedOutput() * transitionMotor.getBusVoltage());
        inputs.setTransitionCurrentAmps(transitionMotor.getOutputCurrent());
    }

    @Override
    protected void setSolenoidValueInternal(Value value) {
        intakeDoubleSolenoid.set(value);
    }

    @Override
    protected void toggleSolenoidValueInternal() {
        intakeDoubleSolenoid.toggle();
    }

    @Override
    protected void setCollectorValueInternal(double value) {
        // NOTE: With Java 21, a built in clamp method is available.
        collectorMotor.set(MathUtil.clamp(value, -1.0, 1.0));
    }

    @Override
    protected void setTransitionValueInternal(double value) {
        // NOTE: With Java 21, a built in clamp method is available.
        transitionMotor.set(MathUtil.clamp(value, -1.0, 1.0));
    }
}
