package frc.robot.subsystem.intake.io;

import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import frc.robot.util.systemtoggle.ToggleableIO;
import lombok.Data;
import org.littletonrobotics.junction.AutoLog;
import org.littletonrobotics.junction.AutoLogOutput;

/** Abstract class representing the hardware I/O for the Intake as it collects game pieces */
public abstract class IntakeIO extends ToggleableIO {
    private static final SystemToggle TOGGLE = SystemToggle.INTAKE;

    protected IntakeIO(SystemToggleStore store) {
        super(TOGGLE, store);
    }

    @AutoLog
    @Data
    public static class IntakeIOInputs {
        // Pneumatics inputs
        /** The {@link Value} state of the solenoid */
        protected Value solenoidValue = Value.kOff;

        /** Whether or not the extension reed switch is closed */
        protected boolean reedSwitchClosed = false;

        // Collector motor inputs
        /** The current position of the collector motor in radians. */
        protected double collectorPositionRad = 0.0;

        /** The velocity of the collector motor in radians per second. */
        protected double collectorVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the collector motor in Volts. */
        protected double collectorAppliedVolts = 0.0;

        /** The current amperage being pulled by the collector motor in Amperes. */
        protected double collectorCurrentAmps = 0.0;

        // Transition motor inputs
        /** The current position of the transition motor in radians. */
        protected double transitionPositionRad = 0.0;

        /** The velocity of the transition motor in radians per second. */
        protected double transitionVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the transition motor in Volts. */
        protected double transitionAppliedVolts = 0.0;

        /** The current amperage being pulled by the transition motor in Amperes. */
        protected double transitionCurrentAmps = 0.0;

        /**
         * Fetches the {@link IntakeIOInputs#solenoidValue} as a {@link Boolean} for AdvantageKit
         * serialization
         *
         * @return The {@link Value} of the {@link IntakeIOInputs#solenoidValue} such that {@link
         *     Value#kOff} -> null and {@link Value#kForward} -> true
         */
        @AutoLogOutput
        protected Boolean getSolenoidValueAsBoolean() {
            return solenoidValue == Value.kOff ? null : (solenoidValue == Value.kForward);
        }
    }

    /**
     * Updates the {@link IntakeIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    protected abstract void updateInputsInternal(IntakeIOInputs inputs);

    /**
     * Update the desired state of the extension solenoid.
     *
     * @param value The desired {@link Value} to run the solenoid at.
     */
    protected abstract void setSolenoidValueInternal(Value value);

    /**
     * Toggle the desired state of the extension solenoid.
     *
     * <p>If the solenoid is set to {@link Value#kOff}, no change is made.
     */
    protected abstract void toggleSolenoidValueInternal();

    /**
     * Set the desired percent speed to run the collector motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    protected abstract void setCollectorValueInternal(double value);

    /**
     * Set the desired percent speed to run the transition motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    protected abstract void setTransitionValueInternal(double value);

    /**
     * Updates the {@link IntakeIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    public void updateInputs(IntakeIOInputs inputs) {
        if (isToggledOn()) {
            updateInputsInternal(inputs);
        }
    }

    /**
     * Update the desired state of the extension solenoid.
     *
     * @param value The desired {@link Value} to run the solenoid at.
     */
    public void setSolenoidValue(Value value) {
        if (isToggledOn()) {
            setSolenoidValueInternal(value);
        }
    }

    /**
     * Toggle the desired state of the extension solenoid.
     *
     * <p>If the solenoid is set to {@link Value#kOff}, no change is made.
     */
    public void toggleSolenoidValue() {
        if (isToggledOn()) {
            toggleSolenoidValueInternal();
        }
    }

    /**
     * Set the desired percent speed to run the collector motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    public void setCollectorValue(double value) {
        if (isToggledOn()) {
            setCollectorValueInternal(value);
        }
    }

    /**
     * Set the desired percent speed to run the transition motor at.
     *
     * @param value The desired percent speed in the range [-1,1]
     */
    public void setTransitionValue(double value) {
        if (isToggledOn()) {
            setTransitionValueInternal(value);
        }
    }
}
