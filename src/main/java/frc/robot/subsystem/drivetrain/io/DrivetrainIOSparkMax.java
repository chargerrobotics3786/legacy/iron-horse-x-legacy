package frc.robot.subsystem.drivetrain.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.revrobotics.CANSparkLowLevel.MotorType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.util.Constants;
import frc.robot.util.featureflag.FeatureFlag;
import frc.robot.util.featureflag.FeatureFlagStore;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.function.Supplier;

/**
 * Implementation of {@link DrivetrainIO} using {@link CANSparkMax} motor controllers for both
 * sides.
 */
@Singleton
public class DrivetrainIOSparkMax extends DrivetrainIO {

    private CANSparkMax leftDriveMotor;
    private CANSparkMax rightDriveMotor;
    private RelativeEncoder leftDriveEncoder;
    private RelativeEncoder rightDriveEncoder;

    private DifferentialDrive differentialDrive;

    private final FeatureFlagStore featureFlagStore;

    /**
     * @param leftPercentSupplier {@link Supplier} for the percentage value to drive the left wheels
     *     at
     * @param rightPercentSupplier {@link Supplier} for the percentage value to drive the left
     *     wheels at
     */
    @Inject
    public DrivetrainIOSparkMax(
            @Named("drivetrainLeftControlSupplier") Supplier<Double> leftPercentSupplier,
            @Named("drivetrainRightControlSupplier") Supplier<Double> rightPercentSupplier,
            FeatureFlagStore featureFlagStore,
            SystemToggleStore systemToggleStore) {
        super(leftPercentSupplier, rightPercentSupplier, systemToggleStore);
        this.featureFlagStore = featureFlagStore;
    }

    protected void initialize() {
        this.leftDriveMotor =
                new CANSparkMax(Constants.Drivetrain.LEFT_DRIVE_MOTOR_ID, MotorType.kBrushless);
        this.leftDriveMotor.setInverted(true);
        this.rightDriveMotor =
                new CANSparkMax(Constants.Drivetrain.RIGHT_DRIVE_MOTOR_ID, MotorType.kBrushless);
        this.rightDriveMotor.setInverted(false);
        this.leftDriveEncoder = leftDriveMotor.getEncoder();
        this.rightDriveEncoder = rightDriveMotor.getEncoder();
        this.differentialDrive = new DifferentialDrive(leftDriveMotor, rightDriveMotor);
    }

    @Override
    protected void updateInputsInternal(DrivetrainIOInputs inputs) {
        inputs.setLeftPositionRad(
                Units.rotationsToRadians(
                        leftDriveEncoder.getPosition()
                                / Constants.Drivetrain.PhysicalCharacteristics.DRIVE_GEAR_RATIO));
        inputs.setLeftVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        leftDriveEncoder.getVelocity()
                                / Constants.Drivetrain.PhysicalCharacteristics.DRIVE_GEAR_RATIO));
        inputs.setLeftAppliedVolts(
                leftDriveMotor.getAppliedOutput() * leftDriveMotor.getBusVoltage());
        inputs.setLeftCurrentAmps(leftDriveMotor.getOutputCurrent());

        inputs.setRightPositionRad(
                Units.rotationsToRadians(
                        rightDriveEncoder.getPosition()
                                / Constants.Drivetrain.PhysicalCharacteristics.DRIVE_GEAR_RATIO));
        inputs.setRightVelocityRadPerSec(
                Units.rotationsPerMinuteToRadiansPerSecond(
                        rightDriveEncoder.getVelocity()
                                / Constants.Drivetrain.PhysicalCharacteristics.DRIVE_GEAR_RATIO));
        inputs.setRightAppliedVolts(
                rightDriveMotor.getAppliedOutput() * rightDriveMotor.getBusVoltage());
        inputs.setRightCurrentAmps(rightDriveMotor.getOutputCurrent());
    }

    @Override
    protected void updateDriveInternal() {
        double speedFactor =
                featureFlagStore.isEnabled(FeatureFlag.TRAINING_MODE).orElse(false)
                        ? 1.0
                        : MathUtil.clamp(Constants.Drivetrain.MAX_SPEED, 0, 1.0);
        differentialDrive.tankDrive(
                leftPercentSupplier.get() * speedFactor, rightPercentSupplier.get() * speedFactor);
    }
}
