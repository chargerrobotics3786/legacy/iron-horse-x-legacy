package frc.robot.subsystem.drivetrain.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.function.Supplier;

/** Implementation of {@link DrivetrainIO} used for Replaying logs. */
@Singleton
public class DrivetrainIOReplay extends DrivetrainIO {

    /**
     * @param leftPercentSupplier {@link Supplier} for the percentage value to drive the left wheels
     *     at
     * @param rightPercentSupplier {@link Supplier} for the percentage value to drive the left
     *     wheels at
     */
    @Inject
    public DrivetrainIOReplay(
            @Named("drivetrainLeftControlSupplier") Supplier<Double> leftPercentSupplier,
            @Named("drivetrainRightControlSupplier") Supplier<Double> rightPercentSupplier,
            SystemToggleStore systemToggleStore) {
        super(leftPercentSupplier, rightPercentSupplier, systemToggleStore);
    }

    @Override
    protected void updateInputsInternal(DrivetrainIOInputs inputs) {
        // Replay implementations have no actual implementation
    }

    @Override
    protected void updateDriveInternal() {
        // Replay implementations have no actual implementation
    }

    @Override
    protected void initialize() {
        /* Replay implementations have no actual implementation */
    }
}
