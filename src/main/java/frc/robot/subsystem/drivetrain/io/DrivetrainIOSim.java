package frc.robot.subsystem.drivetrain.io;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.system.plant.DCMotor;
import edu.wpi.first.wpilibj.simulation.DifferentialDrivetrainSim;
import frc.robot.util.Constants;
import frc.robot.util.modules.MetadataModule.PeriodProvider;
import frc.robot.util.systemtoggle.SystemToggleStore;
import java.util.function.Supplier;

/** Implementation of {@link DrivetrainIO} used in simulation. */
@Singleton
public class DrivetrainIOSim extends DrivetrainIO {

    private static final double SIMULATOR_PERCENT_TO_VOLTAGE_RATIO = 12.0;

    private DifferentialDrivetrainSim sim;

    private double leftAppliedVolts = 0.0;
    private double rightAppliedVolts = 0.0;

    private final PeriodProvider periodProvider;

    /**
     * @param leftPercentSupplier {@link Supplier} for the percentage value to drive the left wheels
     *     at
     * @param rightPercentSupplier {@link Supplier} for the percentage value to drive the left
     *     wheels at
     */
    @Inject
    public DrivetrainIOSim(
            @Named("drivetrainLeftControlSupplier") Supplier<Double> leftPercentSupplier,
            @Named("drivetrainRightControlSupplier") Supplier<Double> rightPercentSupplier,
            SystemToggleStore systemToggleStore,
            PeriodProvider periodProvider) {
        super(leftPercentSupplier, rightPercentSupplier, systemToggleStore);
        this.periodProvider = periodProvider;
    }

    protected void initialize() {
        sim =
                new DifferentialDrivetrainSim(
                        DCMotor.getNEO(1),
                        Constants.Drivetrain.PhysicalCharacteristics.DRIVE_GEAR_RATIO,
                        Constants.Drivetrain.PhysicalCharacteristics.ROBOT_MOMENT_OF_INERTIA,
                        Constants.Drivetrain.PhysicalCharacteristics.ROBOT_MASS,
                        Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS,
                        Constants.Drivetrain.PhysicalCharacteristics.TRACK_WIDTH,
                        Constants.Drivetrain.PhysicalCharacteristics.MEASUREMENT_NOISE_STD_DEVS);
    }

    @Override
    protected void updateInputsInternal(DrivetrainIOInputs inputs) {
        if (isToggledOn()) {
            sim.update(periodProvider.getPeriod());
            inputs.setLeftPositionRad(
                    sim.getLeftPositionMeters()
                            / Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS);
            inputs.setLeftVelocityRadPerSec(
                    sim.getLeftVelocityMetersPerSecond()
                            / Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS);
            inputs.setLeftAppliedVolts(leftAppliedVolts);
            inputs.setLeftCurrentAmps(sim.getLeftCurrentDrawAmps());

            inputs.setRightPositionRad(
                    sim.getRightPositionMeters()
                            / Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS);
            inputs.setRightVelocityRadPerSec(
                    sim.getRightVelocityMetersPerSecond()
                            / Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS);
            inputs.setRightAppliedVolts(rightAppliedVolts);
            inputs.setRightCurrentAmps(sim.getRightCurrentDrawAmps());
            inputs.setGyroYaw(sim.getHeading());
        }
    }

    @Override
    public void updateDriveInternal() {
        leftAppliedVolts =
                (leftPercentSupplier.get() * SIMULATOR_PERCENT_TO_VOLTAGE_RATIO)
                        * MathUtil.clamp(Constants.Drivetrain.MAX_SPEED, 0, 1.0);
        rightAppliedVolts =
                (rightPercentSupplier.get() * SIMULATOR_PERCENT_TO_VOLTAGE_RATIO)
                        * MathUtil.clamp(Constants.Drivetrain.MAX_SPEED, 0, 1.0);

        sim.setInputs(leftAppliedVolts, rightAppliedVolts);
    }
}
