package frc.robot.subsystem.drivetrain.io;

import edu.wpi.first.math.geometry.Rotation2d;
import frc.robot.util.systemtoggle.SystemToggle;
import frc.robot.util.systemtoggle.SystemToggleStore;
import frc.robot.util.systemtoggle.ToggleableIO;
import java.util.function.Supplier;
import lombok.Data;
import lombok.Getter;
import org.littletonrobotics.junction.AutoLog;

/** Abstract class representing the hardware I/O for the Drivetrain */
public abstract class DrivetrainIO extends ToggleableIO {
    private static final SystemToggle TOGGLE = SystemToggle.DRIVETRAIN;

    @AutoLog
    @Data
    public static class DrivetrainIOInputs {
        /** The current position of the left drive motor in radians. */
        protected double leftPositionRad = 0.0;

        /** The current velocity of the left drive motor in radians per second. */
        protected double leftVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the left drive motor in Volts. */
        protected double leftAppliedVolts = 0.0;

        /** The current amperage being pulled by the left drive motor in Amperes. */
        protected double leftCurrentAmps = 0.0;

        /** The current position of the right drive motor in radians. */
        protected double rightPositionRad = 0.0;

        /** The current velocity of the right drive motor in radians per second. */
        protected double rightVelocityRadPerSec = 0.0;

        /** The current voltage being sent to the right drive motor in Volts. */
        protected double rightAppliedVolts = 0.0;

        /** The current amperage being pulled by the right drive motor in Amperes. */
        protected double rightCurrentAmps = 0.0;

        /** The current yaw of the robot as a {@link Rotation2d}. */
        protected Rotation2d gyroYaw = new Rotation2d();
    }

    /** The current left-hand side, drive percent {@link Supplier} */
    @Getter protected Supplier<Double> leftPercentSupplier;

    /** The current right-hand side, drive percent {@link Supplier} */
    @Getter protected Supplier<Double> rightPercentSupplier;

    /**
     * @param leftPercentSupplier {@link Supplier} for the percentage value to drive the left wheels
     *     at
     * @param rightPercentSupplier {@link Supplier} for the percentage value to drive the left
     *     wheels at
     */
    protected DrivetrainIO(
            Supplier<Double> leftPercentSupplier,
            Supplier<Double> rightPercentSupplier,
            SystemToggleStore store) {
        super(TOGGLE, store);
        this.leftPercentSupplier = leftPercentSupplier;
        this.rightPercentSupplier = rightPercentSupplier;
    }

    /**
     * Updates the {@link DrivetrainIOInputs} values based on the state of the hardware
     *
     * @param inputs The inputs object to update values of
     */
    public void updateInputs(DrivetrainIOInputs inputs) {
        if (isToggledOn()) {
            updateInputsInternal(inputs);
        }
    }

    protected abstract void updateInputsInternal(DrivetrainIOInputs inputs);

    /**
     * Updates the drivetrain state and controls the hardware.
     *
     * <p>This should be run once every periodic loop of the Drivetrain
     */
    public void updateDrive() {
        if (isToggledOn()) {
            updateDriveInternal();
        }
    }

    protected abstract void updateDriveInternal();
}
