package frc.robot.subsystem.drivetrain;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelPositions;
import edu.wpi.first.networktables.BooleanPublisher;
import edu.wpi.first.networktables.BooleanSubscriber;
import edu.wpi.first.networktables.NetworkTableEvent.Kind;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.RobotBase;
import frc.robot.subsystem.drivetrain.io.DrivetrainIO;
import frc.robot.subsystem.drivetrain.io.DrivetrainIOInputsAutoLogged;
import frc.robot.util.Constants;
import frc.robot.util.io.IOSubsystem;
import frc.robot.util.io.IOType;
import java.util.EnumSet;
import java.util.Map;
import lombok.EqualsAndHashCode;
import org.littletonrobotics.junction.AutoLogOutput;
import org.littletonrobotics.junction.Logger;
import org.slf4j.LoggerFactory;

/** Subsystem to run the robot's Drivetrain and maintain odometry */
@EqualsAndHashCode(callSuper = true)
@Singleton
public class DrivetrainSubsystem extends IOSubsystem<DrivetrainIO> {
    private static final org.slf4j.Logger LOGGER =
            LoggerFactory.getLogger(DrivetrainSubsystem.class);

    private final DrivetrainIOInputsAutoLogged inputs;
    private final DifferentialDriveOdometry odometry;

    /**
     * @param drivetrainIO The {@link DrivetrainIO} to use when interfacing with hardware
     */
    @Inject
    public DrivetrainSubsystem(
            IOType ioType,
            Map<IOType, DrivetrainIO> drivetrainIO,
            @Named("resetRobotPosePublisher") BooleanPublisher resetRobotPosePublisher,
            @Named("resetRobotPoseSubscriber") BooleanSubscriber resetRobotPoseSubscriber) {
        super(drivetrainIO.get(ioType));
        this.inputs = new DrivetrainIOInputsAutoLogged();
        this.odometry = new DifferentialDriveOdometry(new Rotation2d(), 0, 0);

        if (RobotBase.isSimulation()) {
            simulationSetup(resetRobotPosePublisher, resetRobotPoseSubscriber);
        }
    }

    private void simulationSetup(BooleanPublisher pub, BooleanSubscriber sub) {
        pub.accept(false);
        NetworkTableInstance.getDefault()
                .addListener(
                        sub,
                        EnumSet.of(Kind.kValueAll),
                        event -> {
                            if (event.valueData.value.getBoolean()) {
                                resetRobotPose();
                                pub.accept(false);
                            }
                        });
    }

    @AutoLogOutput
    public Pose2d getOdometryPose2d() {
        return odometry.getPoseMeters();
    }

    private void resetRobotPose() {
        if (RobotBase.isSimulation()) {
            LOGGER.atInfo().setMessage("Resetting Robot Pose").log();
            odometry.resetPosition(
                    Rotation2d.fromRadians(0),
                    new DifferentialDriveWheelPositions(0, 0),
                    new Pose2d(0, 0, Rotation2d.fromRadians(0)));
        } else {
            LOGGER.atWarn()
                    .setMessage("Resetting Robot Pose is not supported outside of simulation.")
                    .log();
        }
    }

    @Override
    public void periodic() {
        this.subsystemIO.updateInputs(inputs);
        Logger.processInputs("Drive", inputs);

        this.subsystemIO.updateDrive();

        odometry.update(
                inputs.getGyroYaw(),
                inputs.getLeftPositionRad()
                        * Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS,
                inputs.getRightPositionRad()
                        * Constants.Drivetrain.PhysicalCharacteristics.WHEEL_RADIUS);
    }
}
