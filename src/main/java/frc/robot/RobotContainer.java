// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.subsystem.drivetrain.DrivetrainSubsystem;
import frc.robot.util.controlscheme.ControlSchemeOption;
import java.util.Set;
import lombok.Getter;

@Singleton
public class RobotContainer {
    private final Compressor compressor;

    @Getter private final DrivetrainSubsystem drivetrainSubsystem;
    @Getter private final Set<SubsystemBase> allSubsystems;

    @Getter private ControlSchemeOption controlSchemeOption;

    @Inject
    public RobotContainer(
            DrivetrainSubsystem drivetrainSubsystem, Set<SubsystemBase> allSubsystems) {
        compressor = new Compressor(PneumaticsModuleType.CTREPCM);
        this.drivetrainSubsystem = drivetrainSubsystem;
        this.allSubsystems = allSubsystems;
    }

    public void enableCompressor() {
        compressor.enableDigital();
    }

    public void disableCompressor() {
        compressor.disable();
    }
}
