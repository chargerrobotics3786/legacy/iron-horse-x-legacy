// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import edu.wpi.first.wpilibj.RobotBase;
import frc.robot.util.modules.FeatureFlagModule;
import frc.robot.util.modules.IOModule;
import frc.robot.util.modules.MetadataModule;
import frc.robot.util.modules.NTModule;
import frc.robot.util.modules.SystemToggleModule;

public final class Main {
    private Main() {}

    public static void main(String... args) {
        Injector injector =
                Guice.createInjector(
                        new FeatureFlagModule(),
                        new SystemToggleModule(),
                        new MetadataModule(),
                        new IOModule(),
                        new NTModule());
        Provider<Robot> robotProvider = injector.getProvider(Robot.class);
        RobotBase.startRobot(robotProvider::get);
    }
}
