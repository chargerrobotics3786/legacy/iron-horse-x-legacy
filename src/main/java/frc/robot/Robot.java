// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.google.inject.Inject;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.command.TestModeCommandService;
import frc.robot.subsystem.drivetrain.DrivetrainSubsystem;
import frc.robot.util.AdvantageKitUtils;
import frc.robot.util.io.IOType;
import frc.robot.util.modules.MetadataModule.PeriodProvider;
import org.littletonrobotics.junction.LoggedRobot;
import org.slf4j.LoggerFactory;

public class Robot extends LoggedRobot {

    public static final boolean IS_REPLAY = false;

    private final RobotContainer robotContainer;

    private final DrivetrainSubsystem drivetrainSubsystem;

    // For simulation only
    private Field2d simulationField;

    private final IOType ioType;

    private final TestModeCommandService testModeCommandService;

    private Command testCommand;

    @Inject
    public Robot(
            RobotContainer robotContainer,
            DrivetrainSubsystem drivetrainSubsystem,
            IOType ioType,
            TestModeCommandService testModeCommandService,
            PeriodProvider periodProvider) {
        super();
        this.robotContainer = robotContainer;
        this.drivetrainSubsystem = drivetrainSubsystem;
        this.ioType = ioType;
        this.testModeCommandService = testModeCommandService;
        periodProvider.setPeriod(this.getPeriod());
    }

    @Override
    public void robotInit() {
        LoggerFactory.getLogger(getClass()).info("Running robotInit...");
        AdvantageKitUtils.initialSetup(ioType);
        setUseTiming(ioType == IOType.REPLAY);
        LoggerFactory.getLogger(getClass()).info("Starting Robot...");
    }

    @Override
    public void robotPeriodic() {
        CommandScheduler.getInstance().run();
    }

    @Override
    public void disabledInit() {
        LoggerFactory.getLogger(getClass()).info("Running disabledInit...");
        robotContainer.disableCompressor();
    }

    @Override
    public void disabledPeriodic() {
        // No operations performed during the disabled state
    }

    @Override
    public void disabledExit() {
        LoggerFactory.getLogger(getClass()).info("Running disabledExit...");
        robotContainer.enableCompressor();
    }

    @Override
    public void driverStationConnected() {
        // No operations require driver station connection
        LoggerFactory.getLogger(getClass()).info("Driver Station connected...");
    }

    @Override
    public void autonomousInit() {
        // This robot does not require autonomous operation
        LoggerFactory.getLogger(getClass()).info("Running autonomousInit...");
    }

    @Override
    public void autonomousPeriodic() {
        // This robot does not require autonomous operation
    }

    @Override
    public void autonomousExit() {
        // This robot does not require autonomous operation
        LoggerFactory.getLogger(getClass()).info("Running autonomousExit...");
    }

    @Override
    public void teleopInit() {
        // All initialization for teleop is currently handled elsewhere
        LoggerFactory.getLogger(getClass()).info("Running teleopInit...");
    }

    @Override
    public void teleopPeriodic() {
        // All periodic work for teleop is currently handled in Subsystems
    }

    @Override
    public void teleopExit() {
        // No work is performed on teleop exit
        LoggerFactory.getLogger(getClass()).info("Running teleopExit...");
    }

    @Override
    public void testInit() {
        LoggerFactory.getLogger(getClass()).info("Running testInit...");
        CommandScheduler.getInstance().cancelAll();
        robotContainer
                .getAllSubsystems()
                .forEach(sys -> CommandScheduler.getInstance().unregisterSubsystem(sys));

        testCommand = testModeCommandService.getTestCommand();
        testCommand.schedule();
    }

    @Override
    public void testPeriodic() {
        // Test command is scheduled during the testInit method
    }

    @Override
    public void testExit() {
        LoggerFactory.getLogger(getClass()).info("Running testExit...");
        if (testCommand != null) {
            testCommand.cancel();
        }
        testCommand = null;
        robotContainer
                .getAllSubsystems()
                .forEach(sys -> CommandScheduler.getInstance().registerSubsystem(sys));
    }

    @Override
    public void simulationInit() {
        LoggerFactory.getLogger(getClass()).info("Running simulationInit...");
        this.simulationField = new Field2d();
    }

    @Override
    public void simulationPeriodic() {
        // Simlation periodic work is covered within each subsystem.
        simulationField.setRobotPose(drivetrainSubsystem.getOdometryPose2d());
        SmartDashboard.putData(simulationField);
    }
}
